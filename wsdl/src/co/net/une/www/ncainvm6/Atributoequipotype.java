/*
 * XML Type:  atributoequipotype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Atributoequipotype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6;


/**
 * An XML atributoequipotype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public interface Atributoequipotype extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Atributoequipotype.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sAF421E091741FB4D19C06A5B0D6144CC").resolveHandle("atributoequipotyped4bbtype");
    
    /**
     * Gets the "NombreAtributo" element
     */
    java.lang.String getNombreAtributo();
    
    /**
     * Gets (as xml) the "NombreAtributo" element
     */
    org.apache.xmlbeans.XmlString xgetNombreAtributo();
    
    /**
     * Sets the "NombreAtributo" element
     */
    void setNombreAtributo(java.lang.String nombreAtributo);
    
    /**
     * Sets (as xml) the "NombreAtributo" element
     */
    void xsetNombreAtributo(org.apache.xmlbeans.XmlString nombreAtributo);
    
    /**
     * Gets the "ValorAtributo" element
     */
    java.lang.String getValorAtributo();
    
    /**
     * Gets (as xml) the "ValorAtributo" element
     */
    org.apache.xmlbeans.XmlString xgetValorAtributo();
    
    /**
     * Sets the "ValorAtributo" element
     */
    void setValorAtributo(java.lang.String valorAtributo);
    
    /**
     * Sets (as xml) the "ValorAtributo" element
     */
    void xsetValorAtributo(org.apache.xmlbeans.XmlString valorAtributo);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static co.net.une.www.ncainvm6.Atributoequipotype newInstance() {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.Atributoequipotype parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.Atributoequipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
