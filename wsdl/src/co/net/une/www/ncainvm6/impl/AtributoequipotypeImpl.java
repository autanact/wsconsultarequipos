/*
 * XML Type:  atributoequipotype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Atributoequipotype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML atributoequipotype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class AtributoequipotypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Atributoequipotype
{
    
    public AtributoequipotypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NOMBREATRIBUTO$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "NombreAtributo");
    private static final javax.xml.namespace.QName VALORATRIBUTO$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "ValorAtributo");
    
    
    /**
     * Gets the "NombreAtributo" element
     */
    public java.lang.String getNombreAtributo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOMBREATRIBUTO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "NombreAtributo" element
     */
    public org.apache.xmlbeans.XmlString xgetNombreAtributo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NOMBREATRIBUTO$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "NombreAtributo" element
     */
    public void setNombreAtributo(java.lang.String nombreAtributo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOMBREATRIBUTO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NOMBREATRIBUTO$0);
            }
            target.setStringValue(nombreAtributo);
        }
    }
    
    /**
     * Sets (as xml) the "NombreAtributo" element
     */
    public void xsetNombreAtributo(org.apache.xmlbeans.XmlString nombreAtributo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NOMBREATRIBUTO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NOMBREATRIBUTO$0);
            }
            target.set(nombreAtributo);
        }
    }
    
    /**
     * Gets the "ValorAtributo" element
     */
    public java.lang.String getValorAtributo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALORATRIBUTO$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ValorAtributo" element
     */
    public org.apache.xmlbeans.XmlString xgetValorAtributo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(VALORATRIBUTO$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ValorAtributo" element
     */
    public void setValorAtributo(java.lang.String valorAtributo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALORATRIBUTO$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VALORATRIBUTO$2);
            }
            target.setStringValue(valorAtributo);
        }
    }
    
    /**
     * Sets (as xml) the "ValorAtributo" element
     */
    public void xsetValorAtributo(org.apache.xmlbeans.XmlString valorAtributo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(VALORATRIBUTO$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(VALORATRIBUTO$2);
            }
            target.set(valorAtributo);
        }
    }
}
