/*
 * XML Type:  equipotype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Equipotype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML equipotype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class EquipotypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Equipotype
{
    
    public EquipotypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SECUENCIA$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Secuencia");
    private static final javax.xml.namespace.QName IDLOCALIZACION$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdLocalizacion");
    private static final javax.xml.namespace.QName IDUNEEQUIPOPADRE$4 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdUneEquipoPadre");
    private static final javax.xml.namespace.QName IDUNEEQUIPO$6 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdUneEquipo");
    private static final javax.xml.namespace.QName ALIASEQUIPO$8 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "AliasEquipo");
    private static final javax.xml.namespace.QName TIPOEQUIPO$10 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "TipoEquipo");
    private static final javax.xml.namespace.QName DESCRIPCIONEQUIPO$12 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "DescripcionEquipo");
    private static final javax.xml.namespace.QName CANALEQUIPO$14 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "CanalEquipo");
    private static final javax.xml.namespace.QName TECNOLOGIA$16 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Tecnologia");
    private static final javax.xml.namespace.QName UMBRALFALLA$18 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "UmbralFalla");
    private static final javax.xml.namespace.QName RELEVANTE$20 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Relevante");
    private static final javax.xml.namespace.QName DIRECCIONELEMENTORED$22 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "DireccionElementoRed");
    
    
    /**
     * Gets the "Secuencia" element
     */
    public java.lang.String getSecuencia()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECUENCIA$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Secuencia" element
     */
    public org.apache.xmlbeans.XmlString xgetSecuencia()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SECUENCIA$0, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "Secuencia" element
     */
    public boolean isNilSecuencia()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SECUENCIA$0, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "Secuencia" element
     */
    public boolean isSetSecuencia()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SECUENCIA$0) != 0;
        }
    }
    
    /**
     * Sets the "Secuencia" element
     */
    public void setSecuencia(java.lang.String secuencia)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECUENCIA$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SECUENCIA$0);
            }
            target.setStringValue(secuencia);
        }
    }
    
    /**
     * Sets (as xml) the "Secuencia" element
     */
    public void xsetSecuencia(org.apache.xmlbeans.XmlString secuencia)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SECUENCIA$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SECUENCIA$0);
            }
            target.set(secuencia);
        }
    }
    
    /**
     * Nils the "Secuencia" element
     */
    public void setNilSecuencia()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SECUENCIA$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SECUENCIA$0);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "Secuencia" element
     */
    public void unsetSecuencia()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SECUENCIA$0, 0);
        }
    }
    
    /**
     * Gets the "IdLocalizacion" element
     */
    public java.lang.String getIdLocalizacion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDLOCALIZACION$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdLocalizacion" element
     */
    public org.apache.xmlbeans.XmlString xgetIdLocalizacion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDLOCALIZACION$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdLocalizacion" element
     */
    public void setIdLocalizacion(java.lang.String idLocalizacion)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDLOCALIZACION$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDLOCALIZACION$2);
            }
            target.setStringValue(idLocalizacion);
        }
    }
    
    /**
     * Sets (as xml) the "IdLocalizacion" element
     */
    public void xsetIdLocalizacion(org.apache.xmlbeans.XmlString idLocalizacion)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDLOCALIZACION$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDLOCALIZACION$2);
            }
            target.set(idLocalizacion);
        }
    }
    
    /**
     * Gets the "IdUneEquipoPadre" element
     */
    public java.lang.String getIdUneEquipoPadre()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDUNEEQUIPOPADRE$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdUneEquipoPadre" element
     */
    public org.apache.xmlbeans.XmlString xgetIdUneEquipoPadre()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDUNEEQUIPOPADRE$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdUneEquipoPadre" element
     */
    public void setIdUneEquipoPadre(java.lang.String idUneEquipoPadre)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDUNEEQUIPOPADRE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDUNEEQUIPOPADRE$4);
            }
            target.setStringValue(idUneEquipoPadre);
        }
    }
    
    /**
     * Sets (as xml) the "IdUneEquipoPadre" element
     */
    public void xsetIdUneEquipoPadre(org.apache.xmlbeans.XmlString idUneEquipoPadre)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDUNEEQUIPOPADRE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDUNEEQUIPOPADRE$4);
            }
            target.set(idUneEquipoPadre);
        }
    }
    
    /**
     * Gets the "IdUneEquipo" element
     */
    public java.lang.String getIdUneEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDUNEEQUIPO$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdUneEquipo" element
     */
    public org.apache.xmlbeans.XmlString xgetIdUneEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDUNEEQUIPO$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdUneEquipo" element
     */
    public void setIdUneEquipo(java.lang.String idUneEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDUNEEQUIPO$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDUNEEQUIPO$6);
            }
            target.setStringValue(idUneEquipo);
        }
    }
    
    /**
     * Sets (as xml) the "IdUneEquipo" element
     */
    public void xsetIdUneEquipo(org.apache.xmlbeans.XmlString idUneEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDUNEEQUIPO$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDUNEEQUIPO$6);
            }
            target.set(idUneEquipo);
        }
    }
    
    /**
     * Gets the "AliasEquipo" element
     */
    public java.lang.String getAliasEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ALIASEQUIPO$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AliasEquipo" element
     */
    public org.apache.xmlbeans.XmlString xgetAliasEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ALIASEQUIPO$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AliasEquipo" element
     */
    public void setAliasEquipo(java.lang.String aliasEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ALIASEQUIPO$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ALIASEQUIPO$8);
            }
            target.setStringValue(aliasEquipo);
        }
    }
    
    /**
     * Sets (as xml) the "AliasEquipo" element
     */
    public void xsetAliasEquipo(org.apache.xmlbeans.XmlString aliasEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ALIASEQUIPO$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ALIASEQUIPO$8);
            }
            target.set(aliasEquipo);
        }
    }
    
    /**
     * Gets the "TipoEquipo" element
     */
    public java.lang.String getTipoEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPOEQUIPO$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "TipoEquipo" element
     */
    public org.apache.xmlbeans.XmlString xgetTipoEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPOEQUIPO$10, 0);
            return target;
        }
    }
    
    /**
     * Sets the "TipoEquipo" element
     */
    public void setTipoEquipo(java.lang.String tipoEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPOEQUIPO$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TIPOEQUIPO$10);
            }
            target.setStringValue(tipoEquipo);
        }
    }
    
    /**
     * Sets (as xml) the "TipoEquipo" element
     */
    public void xsetTipoEquipo(org.apache.xmlbeans.XmlString tipoEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPOEQUIPO$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TIPOEQUIPO$10);
            }
            target.set(tipoEquipo);
        }
    }
    
    /**
     * Gets the "DescripcionEquipo" element
     */
    public java.lang.String getDescripcionEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPCIONEQUIPO$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "DescripcionEquipo" element
     */
    public org.apache.xmlbeans.XmlString xgetDescripcionEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPCIONEQUIPO$12, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "DescripcionEquipo" element
     */
    public boolean isNilDescripcionEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPCIONEQUIPO$12, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "DescripcionEquipo" element
     */
    public boolean isSetDescripcionEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DESCRIPCIONEQUIPO$12) != 0;
        }
    }
    
    /**
     * Sets the "DescripcionEquipo" element
     */
    public void setDescripcionEquipo(java.lang.String descripcionEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPCIONEQUIPO$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESCRIPCIONEQUIPO$12);
            }
            target.setStringValue(descripcionEquipo);
        }
    }
    
    /**
     * Sets (as xml) the "DescripcionEquipo" element
     */
    public void xsetDescripcionEquipo(org.apache.xmlbeans.XmlString descripcionEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPCIONEQUIPO$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DESCRIPCIONEQUIPO$12);
            }
            target.set(descripcionEquipo);
        }
    }
    
    /**
     * Nils the "DescripcionEquipo" element
     */
    public void setNilDescripcionEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPCIONEQUIPO$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DESCRIPCIONEQUIPO$12);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "DescripcionEquipo" element
     */
    public void unsetDescripcionEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DESCRIPCIONEQUIPO$12, 0);
        }
    }
    
    /**
     * Gets the "CanalEquipo" element
     */
    public java.lang.String getCanalEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CANALEQUIPO$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CanalEquipo" element
     */
    public org.apache.xmlbeans.XmlString xgetCanalEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CANALEQUIPO$14, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "CanalEquipo" element
     */
    public boolean isNilCanalEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CANALEQUIPO$14, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "CanalEquipo" element
     */
    public boolean isSetCanalEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CANALEQUIPO$14) != 0;
        }
    }
    
    /**
     * Sets the "CanalEquipo" element
     */
    public void setCanalEquipo(java.lang.String canalEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CANALEQUIPO$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CANALEQUIPO$14);
            }
            target.setStringValue(canalEquipo);
        }
    }
    
    /**
     * Sets (as xml) the "CanalEquipo" element
     */
    public void xsetCanalEquipo(org.apache.xmlbeans.XmlString canalEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CANALEQUIPO$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CANALEQUIPO$14);
            }
            target.set(canalEquipo);
        }
    }
    
    /**
     * Nils the "CanalEquipo" element
     */
    public void setNilCanalEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CANALEQUIPO$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CANALEQUIPO$14);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "CanalEquipo" element
     */
    public void unsetCanalEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CANALEQUIPO$14, 0);
        }
    }
    
    /**
     * Gets the "Tecnologia" element
     */
    public java.lang.String getTecnologia()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TECNOLOGIA$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Tecnologia" element
     */
    public org.apache.xmlbeans.XmlString xgetTecnologia()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TECNOLOGIA$16, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "Tecnologia" element
     */
    public boolean isNilTecnologia()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TECNOLOGIA$16, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "Tecnologia" element
     */
    public boolean isSetTecnologia()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TECNOLOGIA$16) != 0;
        }
    }
    
    /**
     * Sets the "Tecnologia" element
     */
    public void setTecnologia(java.lang.String tecnologia)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TECNOLOGIA$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TECNOLOGIA$16);
            }
            target.setStringValue(tecnologia);
        }
    }
    
    /**
     * Sets (as xml) the "Tecnologia" element
     */
    public void xsetTecnologia(org.apache.xmlbeans.XmlString tecnologia)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TECNOLOGIA$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TECNOLOGIA$16);
            }
            target.set(tecnologia);
        }
    }
    
    /**
     * Nils the "Tecnologia" element
     */
    public void setNilTecnologia()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TECNOLOGIA$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TECNOLOGIA$16);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "Tecnologia" element
     */
    public void unsetTecnologia()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TECNOLOGIA$16, 0);
        }
    }
    
    /**
     * Gets the "UmbralFalla" element
     */
    public java.lang.String getUmbralFalla()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UMBRALFALLA$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "UmbralFalla" element
     */
    public org.apache.xmlbeans.XmlString xgetUmbralFalla()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(UMBRALFALLA$18, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "UmbralFalla" element
     */
    public boolean isNilUmbralFalla()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(UMBRALFALLA$18, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "UmbralFalla" element
     */
    public boolean isSetUmbralFalla()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UMBRALFALLA$18) != 0;
        }
    }
    
    /**
     * Sets the "UmbralFalla" element
     */
    public void setUmbralFalla(java.lang.String umbralFalla)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UMBRALFALLA$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UMBRALFALLA$18);
            }
            target.setStringValue(umbralFalla);
        }
    }
    
    /**
     * Sets (as xml) the "UmbralFalla" element
     */
    public void xsetUmbralFalla(org.apache.xmlbeans.XmlString umbralFalla)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(UMBRALFALLA$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(UMBRALFALLA$18);
            }
            target.set(umbralFalla);
        }
    }
    
    /**
     * Nils the "UmbralFalla" element
     */
    public void setNilUmbralFalla()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(UMBRALFALLA$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(UMBRALFALLA$18);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "UmbralFalla" element
     */
    public void unsetUmbralFalla()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UMBRALFALLA$18, 0);
        }
    }
    
    /**
     * Gets the "Relevante" element
     */
    public java.lang.String getRelevante()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RELEVANTE$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Relevante" element
     */
    public org.apache.xmlbeans.XmlString xgetRelevante()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RELEVANTE$20, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "Relevante" element
     */
    public boolean isNilRelevante()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RELEVANTE$20, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "Relevante" element
     */
    public boolean isSetRelevante()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RELEVANTE$20) != 0;
        }
    }
    
    /**
     * Sets the "Relevante" element
     */
    public void setRelevante(java.lang.String relevante)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RELEVANTE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RELEVANTE$20);
            }
            target.setStringValue(relevante);
        }
    }
    
    /**
     * Sets (as xml) the "Relevante" element
     */
    public void xsetRelevante(org.apache.xmlbeans.XmlString relevante)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RELEVANTE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(RELEVANTE$20);
            }
            target.set(relevante);
        }
    }
    
    /**
     * Nils the "Relevante" element
     */
    public void setNilRelevante()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RELEVANTE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(RELEVANTE$20);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "Relevante" element
     */
    public void unsetRelevante()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RELEVANTE$20, 0);
        }
    }
    
    /**
     * Gets the "DireccionElementoRed" element
     */
    public java.lang.String getDireccionElementoRed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DIRECCIONELEMENTORED$22, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "DireccionElementoRed" element
     */
    public org.apache.xmlbeans.XmlString xgetDireccionElementoRed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DIRECCIONELEMENTORED$22, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "DireccionElementoRed" element
     */
    public boolean isNilDireccionElementoRed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DIRECCIONELEMENTORED$22, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "DireccionElementoRed" element
     */
    public boolean isSetDireccionElementoRed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DIRECCIONELEMENTORED$22) != 0;
        }
    }
    
    /**
     * Sets the "DireccionElementoRed" element
     */
    public void setDireccionElementoRed(java.lang.String direccionElementoRed)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DIRECCIONELEMENTORED$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DIRECCIONELEMENTORED$22);
            }
            target.setStringValue(direccionElementoRed);
        }
    }
    
    /**
     * Sets (as xml) the "DireccionElementoRed" element
     */
    public void xsetDireccionElementoRed(org.apache.xmlbeans.XmlString direccionElementoRed)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DIRECCIONELEMENTORED$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DIRECCIONELEMENTORED$22);
            }
            target.set(direccionElementoRed);
        }
    }
    
    /**
     * Nils the "DireccionElementoRed" element
     */
    public void setNilDireccionElementoRed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DIRECCIONELEMENTORED$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DIRECCIONELEMENTORED$22);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "DireccionElementoRed" element
     */
    public void unsetDireccionElementoRed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DIRECCIONELEMENTORED$22, 0);
        }
    }
}
