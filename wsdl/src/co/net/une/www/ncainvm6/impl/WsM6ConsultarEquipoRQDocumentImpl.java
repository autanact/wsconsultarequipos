/*
 * An XML document type.
 * Localname: WsM6ConsultarEquipo-RQ
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQDocument
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * A document containing one WsM6ConsultarEquipo-RQ(@http://www.une.net.co/ncaInvM6) element.
 *
 * This is a complex type.
 */
public class WsM6ConsultarEquipoRQDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQDocument
{
    
    public WsM6ConsultarEquipoRQDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName WSM6CONSULTAREQUIPORQ$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "WsM6ConsultarEquipo-RQ");
    
    
    /**
     * Gets the "WsM6ConsultarEquipo-RQ" element
     */
    public co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType getWsM6ConsultarEquipoRQ()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType target = null;
            target = (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType)get_store().find_element_user(WSM6CONSULTAREQUIPORQ$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "WsM6ConsultarEquipo-RQ" element
     */
    public void setWsM6ConsultarEquipoRQ(co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType wsM6ConsultarEquipoRQ)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType target = null;
            target = (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType)get_store().find_element_user(WSM6CONSULTAREQUIPORQ$0, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType)get_store().add_element_user(WSM6CONSULTAREQUIPORQ$0);
            }
            target.set(wsM6ConsultarEquipoRQ);
        }
    }
    
    /**
     * Appends and returns a new empty "WsM6ConsultarEquipo-RQ" element
     */
    public co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType addNewWsM6ConsultarEquipoRQ()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType target = null;
            target = (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType)get_store().add_element_user(WSM6CONSULTAREQUIPORQ$0);
            return target;
        }
    }
}
