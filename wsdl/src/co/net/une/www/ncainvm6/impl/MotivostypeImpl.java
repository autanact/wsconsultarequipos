/*
 * XML Type:  motivostype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Motivostype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML motivostype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class MotivostypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Motivostype
{
    
    public MotivostypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName MOTIVO$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "motivo");
    
    
    /**
     * Gets array of all "motivo" elements
     */
    public java.lang.String[] getMotivoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(MOTIVO$0, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "motivo" element
     */
    public java.lang.String getMotivoArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MOTIVO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "motivo" elements
     */
    public org.apache.xmlbeans.XmlString[] xgetMotivoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(MOTIVO$0, targetList);
            org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "motivo" element
     */
    public org.apache.xmlbeans.XmlString xgetMotivoArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MOTIVO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return (org.apache.xmlbeans.XmlString)target;
        }
    }
    
    /**
     * Returns number of "motivo" element
     */
    public int sizeOfMotivoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MOTIVO$0);
        }
    }
    
    /**
     * Sets array of all "motivo" element
     */
    public void setMotivoArray(java.lang.String[] motivoArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(motivoArray, MOTIVO$0);
        }
    }
    
    /**
     * Sets ith "motivo" element
     */
    public void setMotivoArray(int i, java.lang.String motivo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MOTIVO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(motivo);
        }
    }
    
    /**
     * Sets (as xml) array of all "motivo" element
     */
    public void xsetMotivoArray(org.apache.xmlbeans.XmlString[]motivoArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(motivoArray, MOTIVO$0);
        }
    }
    
    /**
     * Sets (as xml) ith "motivo" element
     */
    public void xsetMotivoArray(int i, org.apache.xmlbeans.XmlString motivo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MOTIVO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(motivo);
        }
    }
    
    /**
     * Inserts the value as the ith "motivo" element
     */
    public void insertMotivo(int i, java.lang.String motivo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(MOTIVO$0, i);
            target.setStringValue(motivo);
        }
    }
    
    /**
     * Appends the value as the last "motivo" element
     */
    public void addMotivo(java.lang.String motivo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MOTIVO$0);
            target.setStringValue(motivo);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "motivo" element
     */
    public org.apache.xmlbeans.XmlString insertNewMotivo(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(MOTIVO$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "motivo" element
     */
    public org.apache.xmlbeans.XmlString addNewMotivo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MOTIVO$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "motivo" element
     */
    public void removeMotivo(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MOTIVO$0, i);
        }
    }
}
