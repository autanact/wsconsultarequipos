/*
 * XML Type:  listanotastype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Listanotastype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML listanotastype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class ListanotastypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Listanotastype
{
    
    public ListanotastypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NOTA$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Nota");
    
    
    /**
     * Gets array of all "Nota" elements
     */
    public co.net.une.www.ncainvm6.Notatype[] getNotaArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(NOTA$0, targetList);
            co.net.une.www.ncainvm6.Notatype[] result = new co.net.une.www.ncainvm6.Notatype[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Nota" element
     */
    public co.net.une.www.ncainvm6.Notatype getNotaArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Notatype target = null;
            target = (co.net.une.www.ncainvm6.Notatype)get_store().find_element_user(NOTA$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Nota" element
     */
    public int sizeOfNotaArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(NOTA$0);
        }
    }
    
    /**
     * Sets array of all "Nota" element
     */
    public void setNotaArray(co.net.une.www.ncainvm6.Notatype[] notaArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(notaArray, NOTA$0);
        }
    }
    
    /**
     * Sets ith "Nota" element
     */
    public void setNotaArray(int i, co.net.une.www.ncainvm6.Notatype nota)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Notatype target = null;
            target = (co.net.une.www.ncainvm6.Notatype)get_store().find_element_user(NOTA$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(nota);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Nota" element
     */
    public co.net.une.www.ncainvm6.Notatype insertNewNota(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Notatype target = null;
            target = (co.net.une.www.ncainvm6.Notatype)get_store().insert_element_user(NOTA$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Nota" element
     */
    public co.net.une.www.ncainvm6.Notatype addNewNota()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Notatype target = null;
            target = (co.net.une.www.ncainvm6.Notatype)get_store().add_element_user(NOTA$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Nota" element
     */
    public void removeNota(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(NOTA$0, i);
        }
    }
}
