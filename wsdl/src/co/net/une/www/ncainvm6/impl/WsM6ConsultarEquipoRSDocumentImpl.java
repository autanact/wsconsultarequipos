/*
 * An XML document type.
 * Localname: WsM6ConsultarEquipo-RS
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSDocument
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * A document containing one WsM6ConsultarEquipo-RS(@http://www.une.net.co/ncaInvM6) element.
 *
 * This is a complex type.
 */
public class WsM6ConsultarEquipoRSDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSDocument
{
    
    public WsM6ConsultarEquipoRSDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName WSM6CONSULTAREQUIPORS$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "WsM6ConsultarEquipo-RS");
    
    
    /**
     * Gets the "WsM6ConsultarEquipo-RS" element
     */
    public co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType getWsM6ConsultarEquipoRS()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType target = null;
            target = (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType)get_store().find_element_user(WSM6CONSULTAREQUIPORS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "WsM6ConsultarEquipo-RS" element
     */
    public void setWsM6ConsultarEquipoRS(co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType wsM6ConsultarEquipoRS)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType target = null;
            target = (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType)get_store().find_element_user(WSM6CONSULTAREQUIPORS$0, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType)get_store().add_element_user(WSM6CONSULTAREQUIPORS$0);
            }
            target.set(wsM6ConsultarEquipoRS);
        }
    }
    
    /**
     * Appends and returns a new empty "WsM6ConsultarEquipo-RS" element
     */
    public co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType addNewWsM6ConsultarEquipoRS()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType target = null;
            target = (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType)get_store().add_element_user(WSM6CONSULTAREQUIPORS$0);
            return target;
        }
    }
}
