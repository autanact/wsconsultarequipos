/*
 * XML Type:  char
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Char
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML char(@http://www.une.net.co/ncaInvM6).
 *
 * This is an atomic type that is a restriction of co.net.une.www.ncainvm6.Char.
 */
public class CharImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements co.net.une.www.ncainvm6.Char
{
    
    public CharImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected CharImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
