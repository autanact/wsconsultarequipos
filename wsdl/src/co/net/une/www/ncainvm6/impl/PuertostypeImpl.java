/*
 * XML Type:  puertostype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Puertostype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML puertostype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class PuertostypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Puertostype
{
    
    public PuertostypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NROPUERTO$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "NroPuerto");
    private static final javax.xml.namespace.QName ESTADO$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Estado");
    private static final javax.xml.namespace.QName RFS$4 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "RFS");
    private static final javax.xml.namespace.QName IDDIRECCION$6 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdDireccion");
    
    
    /**
     * Gets the "NroPuerto" element
     */
    public java.lang.String getNroPuerto()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NROPUERTO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "NroPuerto" element
     */
    public org.apache.xmlbeans.XmlString xgetNroPuerto()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NROPUERTO$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "NroPuerto" element
     */
    public void setNroPuerto(java.lang.String nroPuerto)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NROPUERTO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NROPUERTO$0);
            }
            target.setStringValue(nroPuerto);
        }
    }
    
    /**
     * Sets (as xml) the "NroPuerto" element
     */
    public void xsetNroPuerto(org.apache.xmlbeans.XmlString nroPuerto)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NROPUERTO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NROPUERTO$0);
            }
            target.set(nroPuerto);
        }
    }
    
    /**
     * Gets the "Estado" element
     */
    public java.lang.String getEstado()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ESTADO$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Estado" element
     */
    public org.apache.xmlbeans.XmlString xgetEstado()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ESTADO$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Estado" element
     */
    public void setEstado(java.lang.String estado)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ESTADO$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ESTADO$2);
            }
            target.setStringValue(estado);
        }
    }
    
    /**
     * Sets (as xml) the "Estado" element
     */
    public void xsetEstado(org.apache.xmlbeans.XmlString estado)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ESTADO$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ESTADO$2);
            }
            target.set(estado);
        }
    }
    
    /**
     * Gets the "RFS" element
     */
    public java.lang.String getRFS()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RFS$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "RFS" element
     */
    public org.apache.xmlbeans.XmlString xgetRFS()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RFS$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "RFS" element
     */
    public void setRFS(java.lang.String rfs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RFS$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RFS$4);
            }
            target.setStringValue(rfs);
        }
    }
    
    /**
     * Sets (as xml) the "RFS" element
     */
    public void xsetRFS(org.apache.xmlbeans.XmlString rfs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RFS$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(RFS$4);
            }
            target.set(rfs);
        }
    }
    
    /**
     * Gets the "IdDireccion" element
     */
    public java.lang.String getIdDireccion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDDIRECCION$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdDireccion" element
     */
    public org.apache.xmlbeans.XmlString xgetIdDireccion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDDIRECCION$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdDireccion" element
     */
    public void setIdDireccion(java.lang.String idDireccion)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDDIRECCION$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDDIRECCION$6);
            }
            target.setStringValue(idDireccion);
        }
    }
    
    /**
     * Sets (as xml) the "IdDireccion" element
     */
    public void xsetIdDireccion(org.apache.xmlbeans.XmlString idDireccion)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDDIRECCION$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDDIRECCION$6);
            }
            target.set(idDireccion);
        }
    }
}
