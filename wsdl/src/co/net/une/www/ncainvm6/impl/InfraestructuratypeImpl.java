/*
 * XML Type:  infraestructuratype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Infraestructuratype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML infraestructuratype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class InfraestructuratypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Infraestructuratype
{
    
    public InfraestructuratypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INFRAESTRUCTURAVERSION$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "InfraestructuraVersion");
    private static final javax.xml.namespace.QName PLANTA$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Planta");
    private static final javax.xml.namespace.QName RFS$4 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "RFS");
    private static final javax.xml.namespace.QName EQUIPOS$6 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Equipos");
    
    
    /**
     * Gets the "InfraestructuraVersion" element
     */
    public java.lang.String getInfraestructuraVersion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INFRAESTRUCTURAVERSION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "InfraestructuraVersion" element
     */
    public org.apache.xmlbeans.XmlString xgetInfraestructuraVersion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(INFRAESTRUCTURAVERSION$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "InfraestructuraVersion" element
     */
    public void setInfraestructuraVersion(java.lang.String infraestructuraVersion)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INFRAESTRUCTURAVERSION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(INFRAESTRUCTURAVERSION$0);
            }
            target.setStringValue(infraestructuraVersion);
        }
    }
    
    /**
     * Sets (as xml) the "InfraestructuraVersion" element
     */
    public void xsetInfraestructuraVersion(org.apache.xmlbeans.XmlString infraestructuraVersion)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(INFRAESTRUCTURAVERSION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(INFRAESTRUCTURAVERSION$0);
            }
            target.set(infraestructuraVersion);
        }
    }
    
    /**
     * Gets the "Planta" element
     */
    public java.lang.String getPlanta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PLANTA$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Planta" element
     */
    public org.apache.xmlbeans.XmlString xgetPlanta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PLANTA$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Planta" element
     */
    public void setPlanta(java.lang.String planta)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PLANTA$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PLANTA$2);
            }
            target.setStringValue(planta);
        }
    }
    
    /**
     * Sets (as xml) the "Planta" element
     */
    public void xsetPlanta(org.apache.xmlbeans.XmlString planta)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PLANTA$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(PLANTA$2);
            }
            target.set(planta);
        }
    }
    
    /**
     * Gets the "RFS" element
     */
    public java.lang.String getRFS()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RFS$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "RFS" element
     */
    public org.apache.xmlbeans.XmlString xgetRFS()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RFS$4, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "RFS" element
     */
    public boolean isNilRFS()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RFS$4, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "RFS" element
     */
    public boolean isSetRFS()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RFS$4) != 0;
        }
    }
    
    /**
     * Sets the "RFS" element
     */
    public void setRFS(java.lang.String rfs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RFS$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RFS$4);
            }
            target.setStringValue(rfs);
        }
    }
    
    /**
     * Sets (as xml) the "RFS" element
     */
    public void xsetRFS(org.apache.xmlbeans.XmlString rfs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RFS$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(RFS$4);
            }
            target.set(rfs);
        }
    }
    
    /**
     * Nils the "RFS" element
     */
    public void setNilRFS()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RFS$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(RFS$4);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "RFS" element
     */
    public void unsetRFS()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RFS$4, 0);
        }
    }
    
    /**
     * Gets the "Equipos" element
     */
    public co.net.une.www.ncainvm6.Listaequipostype getEquipos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaequipostype target = null;
            target = (co.net.une.www.ncainvm6.Listaequipostype)get_store().find_element_user(EQUIPOS$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Equipos" element
     */
    public void setEquipos(co.net.une.www.ncainvm6.Listaequipostype equipos)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaequipostype target = null;
            target = (co.net.une.www.ncainvm6.Listaequipostype)get_store().find_element_user(EQUIPOS$6, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Listaequipostype)get_store().add_element_user(EQUIPOS$6);
            }
            target.set(equipos);
        }
    }
    
    /**
     * Appends and returns a new empty "Equipos" element
     */
    public co.net.une.www.ncainvm6.Listaequipostype addNewEquipos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaequipostype target = null;
            target = (co.net.une.www.ncainvm6.Listaequipostype)get_store().add_element_user(EQUIPOS$6);
            return target;
        }
    }
}
