/*
 * XML Type:  boundedString14
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.BoundedString14
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML boundedString14(@http://www.une.net.co/ncaInvM6).
 *
 * This is an atomic type that is a restriction of co.net.une.www.ncainvm6.BoundedString14.
 */
public class BoundedString14Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements co.net.une.www.ncainvm6.BoundedString14
{
    
    public BoundedString14Impl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected BoundedString14Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
