/*
 * XML Type:  equipoCPEtype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.EquipoCPEtype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML equipoCPEtype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class EquipoCPEtypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.EquipoCPEtype
{
    
    public EquipoCPEtypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName MARCA$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Marca");
    private static final javax.xml.namespace.QName MODELO$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Modelo");
    private static final javax.xml.namespace.QName SERIAL$4 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Serial");
    private static final javax.xml.namespace.QName ESTADO$6 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Estado");
    private static final javax.xml.namespace.QName TIPO$8 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Tipo");
    private static final javax.xml.namespace.QName IDSAP$10 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdSap");
    private static final javax.xml.namespace.QName MAC1$12 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "MAC1");
    private static final javax.xml.namespace.QName MAC2$14 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "MAC2");
    private static final javax.xml.namespace.QName TIPODECO$16 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "TipoDeco");
    private static final javax.xml.namespace.QName TIPOMODEM$18 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "TipoModem");
    private static final javax.xml.namespace.QName PUERTOS$20 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Puertos");
    
    
    /**
     * Gets the "Marca" element
     */
    public java.lang.String getMarca()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MARCA$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Marca" element
     */
    public org.apache.xmlbeans.XmlString xgetMarca()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MARCA$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Marca" element
     */
    public void setMarca(java.lang.String marca)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MARCA$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MARCA$0);
            }
            target.setStringValue(marca);
        }
    }
    
    /**
     * Sets (as xml) the "Marca" element
     */
    public void xsetMarca(org.apache.xmlbeans.XmlString marca)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MARCA$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MARCA$0);
            }
            target.set(marca);
        }
    }
    
    /**
     * Gets the "Modelo" element
     */
    public java.lang.String getModelo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MODELO$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Modelo" element
     */
    public org.apache.xmlbeans.XmlString xgetModelo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MODELO$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Modelo" element
     */
    public void setModelo(java.lang.String modelo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MODELO$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MODELO$2);
            }
            target.setStringValue(modelo);
        }
    }
    
    /**
     * Sets (as xml) the "Modelo" element
     */
    public void xsetModelo(org.apache.xmlbeans.XmlString modelo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MODELO$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MODELO$2);
            }
            target.set(modelo);
        }
    }
    
    /**
     * Gets the "Serial" element
     */
    public java.lang.String getSerial()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SERIAL$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Serial" element
     */
    public org.apache.xmlbeans.XmlString xgetSerial()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SERIAL$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Serial" element
     */
    public void setSerial(java.lang.String serial)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SERIAL$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SERIAL$4);
            }
            target.setStringValue(serial);
        }
    }
    
    /**
     * Sets (as xml) the "Serial" element
     */
    public void xsetSerial(org.apache.xmlbeans.XmlString serial)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SERIAL$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SERIAL$4);
            }
            target.set(serial);
        }
    }
    
    /**
     * Gets the "Estado" element
     */
    public java.lang.String getEstado()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ESTADO$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Estado" element
     */
    public org.apache.xmlbeans.XmlString xgetEstado()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ESTADO$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Estado" element
     */
    public void setEstado(java.lang.String estado)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ESTADO$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ESTADO$6);
            }
            target.setStringValue(estado);
        }
    }
    
    /**
     * Sets (as xml) the "Estado" element
     */
    public void xsetEstado(org.apache.xmlbeans.XmlString estado)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ESTADO$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ESTADO$6);
            }
            target.set(estado);
        }
    }
    
    /**
     * Gets the "Tipo" element
     */
    public java.lang.String getTipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPO$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Tipo" element
     */
    public org.apache.xmlbeans.XmlString xgetTipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPO$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Tipo" element
     */
    public void setTipo(java.lang.String tipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPO$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TIPO$8);
            }
            target.setStringValue(tipo);
        }
    }
    
    /**
     * Sets (as xml) the "Tipo" element
     */
    public void xsetTipo(org.apache.xmlbeans.XmlString tipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPO$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TIPO$8);
            }
            target.set(tipo);
        }
    }
    
    /**
     * Gets the "IdSap" element
     */
    public java.lang.String getIdSap()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSAP$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdSap" element
     */
    public org.apache.xmlbeans.XmlString xgetIdSap()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDSAP$10, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdSap" element
     */
    public void setIdSap(java.lang.String idSap)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSAP$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSAP$10);
            }
            target.setStringValue(idSap);
        }
    }
    
    /**
     * Sets (as xml) the "IdSap" element
     */
    public void xsetIdSap(org.apache.xmlbeans.XmlString idSap)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDSAP$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDSAP$10);
            }
            target.set(idSap);
        }
    }
    
    /**
     * Gets the "MAC1" element
     */
    public java.lang.String getMAC1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MAC1$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MAC1" element
     */
    public org.apache.xmlbeans.XmlString xgetMAC1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MAC1$12, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "MAC1" element
     */
    public boolean isNilMAC1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MAC1$12, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "MAC1" element
     */
    public boolean isSetMAC1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MAC1$12) != 0;
        }
    }
    
    /**
     * Sets the "MAC1" element
     */
    public void setMAC1(java.lang.String mac1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MAC1$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MAC1$12);
            }
            target.setStringValue(mac1);
        }
    }
    
    /**
     * Sets (as xml) the "MAC1" element
     */
    public void xsetMAC1(org.apache.xmlbeans.XmlString mac1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MAC1$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MAC1$12);
            }
            target.set(mac1);
        }
    }
    
    /**
     * Nils the "MAC1" element
     */
    public void setNilMAC1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MAC1$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MAC1$12);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "MAC1" element
     */
    public void unsetMAC1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MAC1$12, 0);
        }
    }
    
    /**
     * Gets the "MAC2" element
     */
    public java.lang.String getMAC2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MAC2$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MAC2" element
     */
    public org.apache.xmlbeans.XmlString xgetMAC2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MAC2$14, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "MAC2" element
     */
    public boolean isNilMAC2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MAC2$14, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "MAC2" element
     */
    public boolean isSetMAC2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MAC2$14) != 0;
        }
    }
    
    /**
     * Sets the "MAC2" element
     */
    public void setMAC2(java.lang.String mac2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MAC2$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MAC2$14);
            }
            target.setStringValue(mac2);
        }
    }
    
    /**
     * Sets (as xml) the "MAC2" element
     */
    public void xsetMAC2(org.apache.xmlbeans.XmlString mac2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MAC2$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MAC2$14);
            }
            target.set(mac2);
        }
    }
    
    /**
     * Nils the "MAC2" element
     */
    public void setNilMAC2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MAC2$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MAC2$14);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "MAC2" element
     */
    public void unsetMAC2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MAC2$14, 0);
        }
    }
    
    /**
     * Gets the "TipoDeco" element
     */
    public java.lang.String getTipoDeco()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPODECO$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "TipoDeco" element
     */
    public org.apache.xmlbeans.XmlString xgetTipoDeco()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPODECO$16, 0);
            return target;
        }
    }
    
    /**
     * Sets the "TipoDeco" element
     */
    public void setTipoDeco(java.lang.String tipoDeco)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPODECO$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TIPODECO$16);
            }
            target.setStringValue(tipoDeco);
        }
    }
    
    /**
     * Sets (as xml) the "TipoDeco" element
     */
    public void xsetTipoDeco(org.apache.xmlbeans.XmlString tipoDeco)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPODECO$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TIPODECO$16);
            }
            target.set(tipoDeco);
        }
    }
    
    /**
     * Gets the "TipoModem" element
     */
    public java.lang.String getTipoModem()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPOMODEM$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "TipoModem" element
     */
    public org.apache.xmlbeans.XmlString xgetTipoModem()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPOMODEM$18, 0);
            return target;
        }
    }
    
    /**
     * Sets the "TipoModem" element
     */
    public void setTipoModem(java.lang.String tipoModem)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPOMODEM$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TIPOMODEM$18);
            }
            target.setStringValue(tipoModem);
        }
    }
    
    /**
     * Sets (as xml) the "TipoModem" element
     */
    public void xsetTipoModem(org.apache.xmlbeans.XmlString tipoModem)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPOMODEM$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TIPOMODEM$18);
            }
            target.set(tipoModem);
        }
    }
    
    /**
     * Gets the "Puertos" element
     */
    public co.net.une.www.ncainvm6.Listapuertostype getPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listapuertostype target = null;
            target = (co.net.une.www.ncainvm6.Listapuertostype)get_store().find_element_user(PUERTOS$20, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "Puertos" element
     */
    public boolean isNilPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listapuertostype target = null;
            target = (co.net.une.www.ncainvm6.Listapuertostype)get_store().find_element_user(PUERTOS$20, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "Puertos" element
     */
    public boolean isSetPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PUERTOS$20) != 0;
        }
    }
    
    /**
     * Sets the "Puertos" element
     */
    public void setPuertos(co.net.une.www.ncainvm6.Listapuertostype puertos)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listapuertostype target = null;
            target = (co.net.une.www.ncainvm6.Listapuertostype)get_store().find_element_user(PUERTOS$20, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Listapuertostype)get_store().add_element_user(PUERTOS$20);
            }
            target.set(puertos);
        }
    }
    
    /**
     * Appends and returns a new empty "Puertos" element
     */
    public co.net.une.www.ncainvm6.Listapuertostype addNewPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listapuertostype target = null;
            target = (co.net.une.www.ncainvm6.Listapuertostype)get_store().add_element_user(PUERTOS$20);
            return target;
        }
    }
    
    /**
     * Nils the "Puertos" element
     */
    public void setNilPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listapuertostype target = null;
            target = (co.net.une.www.ncainvm6.Listapuertostype)get_store().find_element_user(PUERTOS$20, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Listapuertostype)get_store().add_element_user(PUERTOS$20);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "Puertos" element
     */
    public void unsetPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PUERTOS$20, 0);
        }
    }
}
