/*
 * XML Type:  puertotype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Puertotype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML puertotype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class PuertotypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Puertotype
{
    
    public PuertotypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDPUERTO$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdPuerto");
    private static final javax.xml.namespace.QName SERVICIOS$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Servicios");
    
    
    /**
     * Gets the "IdPuerto" element
     */
    public java.lang.String getIdPuerto()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDPUERTO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdPuerto" element
     */
    public org.apache.xmlbeans.XmlString xgetIdPuerto()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDPUERTO$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdPuerto" element
     */
    public void setIdPuerto(java.lang.String idPuerto)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDPUERTO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDPUERTO$0);
            }
            target.setStringValue(idPuerto);
        }
    }
    
    /**
     * Sets (as xml) the "IdPuerto" element
     */
    public void xsetIdPuerto(org.apache.xmlbeans.XmlString idPuerto)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDPUERTO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDPUERTO$0);
            }
            target.set(idPuerto);
        }
    }
    
    /**
     * Gets the "Servicios" element
     */
    public co.net.une.www.ncainvm6.Listaservicetype getServicios()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaservicetype target = null;
            target = (co.net.une.www.ncainvm6.Listaservicetype)get_store().find_element_user(SERVICIOS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Servicios" element
     */
    public void setServicios(co.net.une.www.ncainvm6.Listaservicetype servicios)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaservicetype target = null;
            target = (co.net.une.www.ncainvm6.Listaservicetype)get_store().find_element_user(SERVICIOS$2, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Listaservicetype)get_store().add_element_user(SERVICIOS$2);
            }
            target.set(servicios);
        }
    }
    
    /**
     * Appends and returns a new empty "Servicios" element
     */
    public co.net.une.www.ncainvm6.Listaservicetype addNewServicios()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaservicetype target = null;
            target = (co.net.une.www.ncainvm6.Listaservicetype)get_store().add_element_user(SERVICIOS$2);
            return target;
        }
    }
}
