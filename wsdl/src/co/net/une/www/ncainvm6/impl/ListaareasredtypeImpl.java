/*
 * XML Type:  listaareasredtype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Listaareasredtype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML listaareasredtype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class ListaareasredtypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Listaareasredtype
{
    
    public ListaareasredtypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName AREARED$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "AreaRed");
    
    
    /**
     * Gets array of all "AreaRed" elements
     */
    public co.net.une.www.ncainvm6.Arearedtype[] getAreaRedArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(AREARED$0, targetList);
            co.net.une.www.ncainvm6.Arearedtype[] result = new co.net.une.www.ncainvm6.Arearedtype[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "AreaRed" element
     */
    public co.net.une.www.ncainvm6.Arearedtype getAreaRedArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Arearedtype target = null;
            target = (co.net.une.www.ncainvm6.Arearedtype)get_store().find_element_user(AREARED$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "AreaRed" element
     */
    public int sizeOfAreaRedArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(AREARED$0);
        }
    }
    
    /**
     * Sets array of all "AreaRed" element
     */
    public void setAreaRedArray(co.net.une.www.ncainvm6.Arearedtype[] areaRedArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(areaRedArray, AREARED$0);
        }
    }
    
    /**
     * Sets ith "AreaRed" element
     */
    public void setAreaRedArray(int i, co.net.une.www.ncainvm6.Arearedtype areaRed)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Arearedtype target = null;
            target = (co.net.une.www.ncainvm6.Arearedtype)get_store().find_element_user(AREARED$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(areaRed);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "AreaRed" element
     */
    public co.net.une.www.ncainvm6.Arearedtype insertNewAreaRed(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Arearedtype target = null;
            target = (co.net.une.www.ncainvm6.Arearedtype)get_store().insert_element_user(AREARED$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "AreaRed" element
     */
    public co.net.une.www.ncainvm6.Arearedtype addNewAreaRed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Arearedtype target = null;
            target = (co.net.une.www.ncainvm6.Arearedtype)get_store().add_element_user(AREARED$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "AreaRed" element
     */
    public void removeAreaRed(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(AREARED$0, i);
        }
    }
}
