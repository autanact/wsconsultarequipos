/*
 * XML Type:  listaserviciostype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Listaserviciostype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML listaserviciostype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class ListaserviciostypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Listaserviciostype
{
    
    public ListaserviciostypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SERVICIOS$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Servicios");
    
    
    /**
     * Gets array of all "Servicios" elements
     */
    public co.net.une.www.ncainvm6.Serviciotype[] getServiciosArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(SERVICIOS$0, targetList);
            co.net.une.www.ncainvm6.Serviciotype[] result = new co.net.une.www.ncainvm6.Serviciotype[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Servicios" element
     */
    public co.net.une.www.ncainvm6.Serviciotype getServiciosArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Serviciotype target = null;
            target = (co.net.une.www.ncainvm6.Serviciotype)get_store().find_element_user(SERVICIOS$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Servicios" element
     */
    public int sizeOfServiciosArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SERVICIOS$0);
        }
    }
    
    /**
     * Sets array of all "Servicios" element
     */
    public void setServiciosArray(co.net.une.www.ncainvm6.Serviciotype[] serviciosArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(serviciosArray, SERVICIOS$0);
        }
    }
    
    /**
     * Sets ith "Servicios" element
     */
    public void setServiciosArray(int i, co.net.une.www.ncainvm6.Serviciotype servicios)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Serviciotype target = null;
            target = (co.net.une.www.ncainvm6.Serviciotype)get_store().find_element_user(SERVICIOS$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(servicios);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Servicios" element
     */
    public co.net.une.www.ncainvm6.Serviciotype insertNewServicios(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Serviciotype target = null;
            target = (co.net.une.www.ncainvm6.Serviciotype)get_store().insert_element_user(SERVICIOS$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Servicios" element
     */
    public co.net.une.www.ncainvm6.Serviciotype addNewServicios()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Serviciotype target = null;
            target = (co.net.une.www.ncainvm6.Serviciotype)get_store().add_element_user(SERVICIOS$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Servicios" element
     */
    public void removeServicios(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SERVICIOS$0, i);
        }
    }
}
