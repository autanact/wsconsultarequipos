/*
 * XML Type:  WsM6ConsultarEquipo-RQ-Type
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML WsM6ConsultarEquipo-RQ-Type(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class WsM6ConsultarEquipoRQTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType
{
    
    public WsM6ConsultarEquipoRQTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName FECHASOLICITUD$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "FechaSolicitud");
    private static final javax.xml.namespace.QName IDUNEEQUIPO$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdUneEquipo");
    private static final javax.xml.namespace.QName TIPOEQUIPO$4 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "TipoEquipo");
    private static final javax.xml.namespace.QName ATRIBUTOS$6 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Atributos");
    
    
    /**
     * Gets the "FechaSolicitud" element
     */
    public co.net.une.www.ncainvm6.UTCDate getFechaSolicitud()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.UTCDate target = null;
            target = (co.net.une.www.ncainvm6.UTCDate)get_store().find_element_user(FECHASOLICITUD$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "FechaSolicitud" element
     */
    public void setFechaSolicitud(co.net.une.www.ncainvm6.UTCDate fechaSolicitud)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.UTCDate target = null;
            target = (co.net.une.www.ncainvm6.UTCDate)get_store().find_element_user(FECHASOLICITUD$0, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.UTCDate)get_store().add_element_user(FECHASOLICITUD$0);
            }
            target.set(fechaSolicitud);
        }
    }
    
    /**
     * Appends and returns a new empty "FechaSolicitud" element
     */
    public co.net.une.www.ncainvm6.UTCDate addNewFechaSolicitud()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.UTCDate target = null;
            target = (co.net.une.www.ncainvm6.UTCDate)get_store().add_element_user(FECHASOLICITUD$0);
            return target;
        }
    }
    
    /**
     * Gets the "IdUneEquipo" element
     */
    public java.lang.String getIdUneEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDUNEEQUIPO$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdUneEquipo" element
     */
    public org.apache.xmlbeans.XmlString xgetIdUneEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDUNEEQUIPO$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdUneEquipo" element
     */
    public void setIdUneEquipo(java.lang.String idUneEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDUNEEQUIPO$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDUNEEQUIPO$2);
            }
            target.setStringValue(idUneEquipo);
        }
    }
    
    /**
     * Sets (as xml) the "IdUneEquipo" element
     */
    public void xsetIdUneEquipo(org.apache.xmlbeans.XmlString idUneEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDUNEEQUIPO$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDUNEEQUIPO$2);
            }
            target.set(idUneEquipo);
        }
    }
    
    /**
     * Gets the "TipoEquipo" element
     */
    public java.lang.String getTipoEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPOEQUIPO$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "TipoEquipo" element
     */
    public org.apache.xmlbeans.XmlString xgetTipoEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPOEQUIPO$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "TipoEquipo" element
     */
    public void setTipoEquipo(java.lang.String tipoEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPOEQUIPO$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TIPOEQUIPO$4);
            }
            target.setStringValue(tipoEquipo);
        }
    }
    
    /**
     * Sets (as xml) the "TipoEquipo" element
     */
    public void xsetTipoEquipo(org.apache.xmlbeans.XmlString tipoEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPOEQUIPO$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TIPOEQUIPO$4);
            }
            target.set(tipoEquipo);
        }
    }
    
    /**
     * Gets the "Atributos" element
     */
    public co.net.une.www.ncainvm6.Listaatributostype getAtributos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaatributostype target = null;
            target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().find_element_user(ATRIBUTOS$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "Atributos" element
     */
    public boolean isNilAtributos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaatributostype target = null;
            target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().find_element_user(ATRIBUTOS$6, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "Atributos" element
     */
    public boolean isSetAtributos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ATRIBUTOS$6) != 0;
        }
    }
    
    /**
     * Sets the "Atributos" element
     */
    public void setAtributos(co.net.une.www.ncainvm6.Listaatributostype atributos)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaatributostype target = null;
            target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().find_element_user(ATRIBUTOS$6, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().add_element_user(ATRIBUTOS$6);
            }
            target.set(atributos);
        }
    }
    
    /**
     * Appends and returns a new empty "Atributos" element
     */
    public co.net.une.www.ncainvm6.Listaatributostype addNewAtributos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaatributostype target = null;
            target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().add_element_user(ATRIBUTOS$6);
            return target;
        }
    }
    
    /**
     * Nils the "Atributos" element
     */
    public void setNilAtributos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaatributostype target = null;
            target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().find_element_user(ATRIBUTOS$6, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().add_element_user(ATRIBUTOS$6);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "Atributos" element
     */
    public void unsetAtributos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ATRIBUTOS$6, 0);
        }
    }
}
