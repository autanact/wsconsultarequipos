/*
 * XML Type:  notatype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Notatype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML notatype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class NotatypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Notatype
{
    
    public NotatypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SEQNOTA$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "SeqNota");
    private static final javax.xml.namespace.QName DESCRIPCIONNOTA$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "DescripcionNota");
    
    
    /**
     * Gets the "SeqNota" element
     */
    public int getSeqNota()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SEQNOTA$0, 0);
            if (target == null)
            {
                return 0;
            }
            return target.getIntValue();
        }
    }
    
    /**
     * Gets (as xml) the "SeqNota" element
     */
    public org.apache.xmlbeans.XmlInt xgetSeqNota()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(SEQNOTA$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "SeqNota" element
     */
    public void setSeqNota(int seqNota)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SEQNOTA$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SEQNOTA$0);
            }
            target.setIntValue(seqNota);
        }
    }
    
    /**
     * Sets (as xml) the "SeqNota" element
     */
    public void xsetSeqNota(org.apache.xmlbeans.XmlInt seqNota)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(SEQNOTA$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlInt)get_store().add_element_user(SEQNOTA$0);
            }
            target.set(seqNota);
        }
    }
    
    /**
     * Gets the "DescripcionNota" element
     */
    public java.lang.String getDescripcionNota()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPCIONNOTA$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "DescripcionNota" element
     */
    public org.apache.xmlbeans.XmlString xgetDescripcionNota()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPCIONNOTA$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "DescripcionNota" element
     */
    public void setDescripcionNota(java.lang.String descripcionNota)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPCIONNOTA$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESCRIPCIONNOTA$2);
            }
            target.setStringValue(descripcionNota);
        }
    }
    
    /**
     * Sets (as xml) the "DescripcionNota" element
     */
    public void xsetDescripcionNota(org.apache.xmlbeans.XmlString descripcionNota)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPCIONNOTA$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DESCRIPCIONNOTA$2);
            }
            target.set(descripcionNota);
        }
    }
}
