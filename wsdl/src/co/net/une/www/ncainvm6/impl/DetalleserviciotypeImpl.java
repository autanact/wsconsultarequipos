/*
 * XML Type:  detalleserviciotype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Detalleserviciotype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML detalleserviciotype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class DetalleserviciotypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Detalleserviciotype
{
    
    public DetalleserviciotypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDSERVICIO$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdServicio");
    
    
    /**
     * Gets array of all "IdServicio" elements
     */
    public java.lang.String[] getIdServicioArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(IDSERVICIO$0, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "IdServicio" element
     */
    public java.lang.String getIdServicioArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSERVICIO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "IdServicio" elements
     */
    public org.apache.xmlbeans.XmlString[] xgetIdServicioArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(IDSERVICIO$0, targetList);
            org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "IdServicio" element
     */
    public org.apache.xmlbeans.XmlString xgetIdServicioArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDSERVICIO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return (org.apache.xmlbeans.XmlString)target;
        }
    }
    
    /**
     * Returns number of "IdServicio" element
     */
    public int sizeOfIdServicioArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IDSERVICIO$0);
        }
    }
    
    /**
     * Sets array of all "IdServicio" element
     */
    public void setIdServicioArray(java.lang.String[] idServicioArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(idServicioArray, IDSERVICIO$0);
        }
    }
    
    /**
     * Sets ith "IdServicio" element
     */
    public void setIdServicioArray(int i, java.lang.String idServicio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSERVICIO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(idServicio);
        }
    }
    
    /**
     * Sets (as xml) array of all "IdServicio" element
     */
    public void xsetIdServicioArray(org.apache.xmlbeans.XmlString[]idServicioArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(idServicioArray, IDSERVICIO$0);
        }
    }
    
    /**
     * Sets (as xml) ith "IdServicio" element
     */
    public void xsetIdServicioArray(int i, org.apache.xmlbeans.XmlString idServicio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDSERVICIO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(idServicio);
        }
    }
    
    /**
     * Inserts the value as the ith "IdServicio" element
     */
    public void insertIdServicio(int i, java.lang.String idServicio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(IDSERVICIO$0, i);
            target.setStringValue(idServicio);
        }
    }
    
    /**
     * Appends the value as the last "IdServicio" element
     */
    public void addIdServicio(java.lang.String idServicio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSERVICIO$0);
            target.setStringValue(idServicio);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "IdServicio" element
     */
    public org.apache.xmlbeans.XmlString insertNewIdServicio(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(IDSERVICIO$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "IdServicio" element
     */
    public org.apache.xmlbeans.XmlString addNewIdServicio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDSERVICIO$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "IdServicio" element
     */
    public void removeIdServicio(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IDSERVICIO$0, i);
        }
    }
}
