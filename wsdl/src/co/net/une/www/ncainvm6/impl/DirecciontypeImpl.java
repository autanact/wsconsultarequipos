/*
 * XML Type:  direcciontype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Direcciontype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML direcciontype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class DirecciontypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Direcciontype
{
    
    public DirecciontypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDDIRECCION$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdDireccion");
    private static final javax.xml.namespace.QName PAIS$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Pais");
    private static final javax.xml.namespace.QName DEPARTAMENTO$4 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Departamento");
    private static final javax.xml.namespace.QName MUNICIPIO$6 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Municipio");
    private static final javax.xml.namespace.QName DIRECCIONNATURAL$8 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "DireccionNatural");
    
    
    /**
     * Gets the "IdDireccion" element
     */
    public java.lang.String getIdDireccion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDDIRECCION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdDireccion" element
     */
    public org.apache.xmlbeans.XmlString xgetIdDireccion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDDIRECCION$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdDireccion" element
     */
    public void setIdDireccion(java.lang.String idDireccion)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDDIRECCION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDDIRECCION$0);
            }
            target.setStringValue(idDireccion);
        }
    }
    
    /**
     * Sets (as xml) the "IdDireccion" element
     */
    public void xsetIdDireccion(org.apache.xmlbeans.XmlString idDireccion)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDDIRECCION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDDIRECCION$0);
            }
            target.set(idDireccion);
        }
    }
    
    /**
     * Gets the "Pais" element
     */
    public java.lang.String getPais()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PAIS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Pais" element
     */
    public org.apache.xmlbeans.XmlString xgetPais()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PAIS$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Pais" element
     */
    public void setPais(java.lang.String pais)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PAIS$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PAIS$2);
            }
            target.setStringValue(pais);
        }
    }
    
    /**
     * Sets (as xml) the "Pais" element
     */
    public void xsetPais(org.apache.xmlbeans.XmlString pais)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PAIS$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(PAIS$2);
            }
            target.set(pais);
        }
    }
    
    /**
     * Gets the "Departamento" element
     */
    public java.lang.String getDepartamento()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DEPARTAMENTO$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Departamento" element
     */
    public org.apache.xmlbeans.XmlString xgetDepartamento()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DEPARTAMENTO$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Departamento" element
     */
    public void setDepartamento(java.lang.String departamento)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DEPARTAMENTO$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DEPARTAMENTO$4);
            }
            target.setStringValue(departamento);
        }
    }
    
    /**
     * Sets (as xml) the "Departamento" element
     */
    public void xsetDepartamento(org.apache.xmlbeans.XmlString departamento)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DEPARTAMENTO$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DEPARTAMENTO$4);
            }
            target.set(departamento);
        }
    }
    
    /**
     * Gets the "Municipio" element
     */
    public java.lang.String getMunicipio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MUNICIPIO$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Municipio" element
     */
    public org.apache.xmlbeans.XmlString xgetMunicipio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MUNICIPIO$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Municipio" element
     */
    public void setMunicipio(java.lang.String municipio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MUNICIPIO$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MUNICIPIO$6);
            }
            target.setStringValue(municipio);
        }
    }
    
    /**
     * Sets (as xml) the "Municipio" element
     */
    public void xsetMunicipio(org.apache.xmlbeans.XmlString municipio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MUNICIPIO$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MUNICIPIO$6);
            }
            target.set(municipio);
        }
    }
    
    /**
     * Gets the "DireccionNatural" element
     */
    public java.lang.String getDireccionNatural()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DIRECCIONNATURAL$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "DireccionNatural" element
     */
    public org.apache.xmlbeans.XmlString xgetDireccionNatural()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DIRECCIONNATURAL$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "DireccionNatural" element
     */
    public void setDireccionNatural(java.lang.String direccionNatural)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DIRECCIONNATURAL$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DIRECCIONNATURAL$8);
            }
            target.setStringValue(direccionNatural);
        }
    }
    
    /**
     * Sets (as xml) the "DireccionNatural" element
     */
    public void xsetDireccionNatural(org.apache.xmlbeans.XmlString direccionNatural)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DIRECCIONNATURAL$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DIRECCIONNATURAL$8);
            }
            target.set(direccionNatural);
        }
    }
}
