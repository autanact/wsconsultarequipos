/*
 * XML Type:  atributotype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Atributotype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML atributotype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class AtributotypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Atributotype
{
    
    public AtributotypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDATRIBUTO$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdAtributo");
    private static final javax.xml.namespace.QName IDATRIBUTOPADRE$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdAtributoPadre");
    private static final javax.xml.namespace.QName NOMBREATRIBUTO$4 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "NombreAtributo");
    private static final javax.xml.namespace.QName VALORATRIBUTO$6 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "ValorAtributo");
    
    
    /**
     * Gets the "IdAtributo" element
     */
    public java.lang.String getIdAtributo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATRIBUTO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdAtributo" element
     */
    public org.apache.xmlbeans.XmlString xgetIdAtributo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDATRIBUTO$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdAtributo" element
     */
    public void setIdAtributo(java.lang.String idAtributo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATRIBUTO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATRIBUTO$0);
            }
            target.setStringValue(idAtributo);
        }
    }
    
    /**
     * Sets (as xml) the "IdAtributo" element
     */
    public void xsetIdAtributo(org.apache.xmlbeans.XmlString idAtributo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDATRIBUTO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDATRIBUTO$0);
            }
            target.set(idAtributo);
        }
    }
    
    /**
     * Gets the "IdAtributoPadre" element
     */
    public java.lang.String getIdAtributoPadre()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATRIBUTOPADRE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdAtributoPadre" element
     */
    public org.apache.xmlbeans.XmlString xgetIdAtributoPadre()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDATRIBUTOPADRE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdAtributoPadre" element
     */
    public void setIdAtributoPadre(java.lang.String idAtributoPadre)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATRIBUTOPADRE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATRIBUTOPADRE$2);
            }
            target.setStringValue(idAtributoPadre);
        }
    }
    
    /**
     * Sets (as xml) the "IdAtributoPadre" element
     */
    public void xsetIdAtributoPadre(org.apache.xmlbeans.XmlString idAtributoPadre)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDATRIBUTOPADRE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDATRIBUTOPADRE$2);
            }
            target.set(idAtributoPadre);
        }
    }
    
    /**
     * Gets the "NombreAtributo" element
     */
    public java.lang.String getNombreAtributo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOMBREATRIBUTO$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "NombreAtributo" element
     */
    public org.apache.xmlbeans.XmlString xgetNombreAtributo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NOMBREATRIBUTO$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "NombreAtributo" element
     */
    public void setNombreAtributo(java.lang.String nombreAtributo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOMBREATRIBUTO$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NOMBREATRIBUTO$4);
            }
            target.setStringValue(nombreAtributo);
        }
    }
    
    /**
     * Sets (as xml) the "NombreAtributo" element
     */
    public void xsetNombreAtributo(org.apache.xmlbeans.XmlString nombreAtributo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NOMBREATRIBUTO$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NOMBREATRIBUTO$4);
            }
            target.set(nombreAtributo);
        }
    }
    
    /**
     * Gets the "ValorAtributo" element
     */
    public java.lang.String getValorAtributo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALORATRIBUTO$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ValorAtributo" element
     */
    public org.apache.xmlbeans.XmlString xgetValorAtributo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(VALORATRIBUTO$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ValorAtributo" element
     */
    public void setValorAtributo(java.lang.String valorAtributo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALORATRIBUTO$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VALORATRIBUTO$6);
            }
            target.setStringValue(valorAtributo);
        }
    }
    
    /**
     * Sets (as xml) the "ValorAtributo" element
     */
    public void xsetValorAtributo(org.apache.xmlbeans.XmlString valorAtributo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(VALORATRIBUTO$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(VALORATRIBUTO$6);
            }
            target.set(valorAtributo);
        }
    }
}
