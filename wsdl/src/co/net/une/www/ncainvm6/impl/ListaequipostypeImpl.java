/*
 * XML Type:  listaequipostype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Listaequipostype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML listaequipostype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class ListaequipostypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Listaequipostype
{
    
    public ListaequipostypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EQUIPO$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Equipo");
    
    
    /**
     * Gets array of all "Equipo" elements
     */
    public co.net.une.www.ncainvm6.Equipotype[] getEquipoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(EQUIPO$0, targetList);
            co.net.une.www.ncainvm6.Equipotype[] result = new co.net.une.www.ncainvm6.Equipotype[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Equipo" element
     */
    public co.net.une.www.ncainvm6.Equipotype getEquipoArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Equipotype target = null;
            target = (co.net.une.www.ncainvm6.Equipotype)get_store().find_element_user(EQUIPO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Equipo" element
     */
    public int sizeOfEquipoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EQUIPO$0);
        }
    }
    
    /**
     * Sets array of all "Equipo" element
     */
    public void setEquipoArray(co.net.une.www.ncainvm6.Equipotype[] equipoArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(equipoArray, EQUIPO$0);
        }
    }
    
    /**
     * Sets ith "Equipo" element
     */
    public void setEquipoArray(int i, co.net.une.www.ncainvm6.Equipotype equipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Equipotype target = null;
            target = (co.net.une.www.ncainvm6.Equipotype)get_store().find_element_user(EQUIPO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(equipo);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Equipo" element
     */
    public co.net.une.www.ncainvm6.Equipotype insertNewEquipo(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Equipotype target = null;
            target = (co.net.une.www.ncainvm6.Equipotype)get_store().insert_element_user(EQUIPO$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Equipo" element
     */
    public co.net.une.www.ncainvm6.Equipotype addNewEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Equipotype target = null;
            target = (co.net.une.www.ncainvm6.Equipotype)get_store().add_element_user(EQUIPO$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Equipo" element
     */
    public void removeEquipo(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EQUIPO$0, i);
        }
    }
}
