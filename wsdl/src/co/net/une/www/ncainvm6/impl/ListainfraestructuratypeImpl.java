/*
 * XML Type:  listainfraestructuratype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Listainfraestructuratype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML listainfraestructuratype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class ListainfraestructuratypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Listainfraestructuratype
{
    
    public ListainfraestructuratypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INFRAESTRUCTURA$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Infraestructura");
    
    
    /**
     * Gets array of all "Infraestructura" elements
     */
    public co.net.une.www.ncainvm6.Infraestructuratype[] getInfraestructuraArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INFRAESTRUCTURA$0, targetList);
            co.net.une.www.ncainvm6.Infraestructuratype[] result = new co.net.une.www.ncainvm6.Infraestructuratype[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Infraestructura" element
     */
    public co.net.une.www.ncainvm6.Infraestructuratype getInfraestructuraArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Infraestructuratype target = null;
            target = (co.net.une.www.ncainvm6.Infraestructuratype)get_store().find_element_user(INFRAESTRUCTURA$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Infraestructura" element
     */
    public int sizeOfInfraestructuraArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INFRAESTRUCTURA$0);
        }
    }
    
    /**
     * Sets array of all "Infraestructura" element
     */
    public void setInfraestructuraArray(co.net.une.www.ncainvm6.Infraestructuratype[] infraestructuraArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(infraestructuraArray, INFRAESTRUCTURA$0);
        }
    }
    
    /**
     * Sets ith "Infraestructura" element
     */
    public void setInfraestructuraArray(int i, co.net.une.www.ncainvm6.Infraestructuratype infraestructura)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Infraestructuratype target = null;
            target = (co.net.une.www.ncainvm6.Infraestructuratype)get_store().find_element_user(INFRAESTRUCTURA$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(infraestructura);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Infraestructura" element
     */
    public co.net.une.www.ncainvm6.Infraestructuratype insertNewInfraestructura(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Infraestructuratype target = null;
            target = (co.net.une.www.ncainvm6.Infraestructuratype)get_store().insert_element_user(INFRAESTRUCTURA$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Infraestructura" element
     */
    public co.net.une.www.ncainvm6.Infraestructuratype addNewInfraestructura()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Infraestructuratype target = null;
            target = (co.net.une.www.ncainvm6.Infraestructuratype)get_store().add_element_user(INFRAESTRUCTURA$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Infraestructura" element
     */
    public void removeInfraestructura(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INFRAESTRUCTURA$0, i);
        }
    }
}
