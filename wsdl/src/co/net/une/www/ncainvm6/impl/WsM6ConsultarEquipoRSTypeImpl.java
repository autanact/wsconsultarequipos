/*
 * XML Type:  WsM6ConsultarEquipo-RS-Type
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML WsM6ConsultarEquipo-RS-Type(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class WsM6ConsultarEquipoRSTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType
{
    
    public WsM6ConsultarEquipoRSTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName FECHASOLICITUD$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "FechaSolicitud");
    private static final javax.xml.namespace.QName IDUNEEQUIPO$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdUneEquipo");
    private static final javax.xml.namespace.QName TIPOEQUIPO$4 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "TipoEquipo");
    private static final javax.xml.namespace.QName CANTIDADPUERTOS$6 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "CantidadPuertos");
    private static final javax.xml.namespace.QName DETALLERESPUESTA$8 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "DetalleRespuesta");
    private static final javax.xml.namespace.QName PUERTOS$10 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Puertos");
    private static final javax.xml.namespace.QName ATRIBUTOS$12 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Atributos");
    
    
    /**
     * Gets the "FechaSolicitud" element
     */
    public co.net.une.www.ncainvm6.UTCDate getFechaSolicitud()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.UTCDate target = null;
            target = (co.net.une.www.ncainvm6.UTCDate)get_store().find_element_user(FECHASOLICITUD$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "FechaSolicitud" element
     */
    public void setFechaSolicitud(co.net.une.www.ncainvm6.UTCDate fechaSolicitud)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.UTCDate target = null;
            target = (co.net.une.www.ncainvm6.UTCDate)get_store().find_element_user(FECHASOLICITUD$0, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.UTCDate)get_store().add_element_user(FECHASOLICITUD$0);
            }
            target.set(fechaSolicitud);
        }
    }
    
    /**
     * Appends and returns a new empty "FechaSolicitud" element
     */
    public co.net.une.www.ncainvm6.UTCDate addNewFechaSolicitud()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.UTCDate target = null;
            target = (co.net.une.www.ncainvm6.UTCDate)get_store().add_element_user(FECHASOLICITUD$0);
            return target;
        }
    }
    
    /**
     * Gets the "IdUneEquipo" element
     */
    public java.lang.String getIdUneEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDUNEEQUIPO$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdUneEquipo" element
     */
    public org.apache.xmlbeans.XmlString xgetIdUneEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDUNEEQUIPO$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdUneEquipo" element
     */
    public void setIdUneEquipo(java.lang.String idUneEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDUNEEQUIPO$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDUNEEQUIPO$2);
            }
            target.setStringValue(idUneEquipo);
        }
    }
    
    /**
     * Sets (as xml) the "IdUneEquipo" element
     */
    public void xsetIdUneEquipo(org.apache.xmlbeans.XmlString idUneEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDUNEEQUIPO$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDUNEEQUIPO$2);
            }
            target.set(idUneEquipo);
        }
    }
    
    /**
     * Gets the "TipoEquipo" element
     */
    public java.lang.String getTipoEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPOEQUIPO$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "TipoEquipo" element
     */
    public org.apache.xmlbeans.XmlString xgetTipoEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPOEQUIPO$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "TipoEquipo" element
     */
    public void setTipoEquipo(java.lang.String tipoEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPOEQUIPO$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TIPOEQUIPO$4);
            }
            target.setStringValue(tipoEquipo);
        }
    }
    
    /**
     * Sets (as xml) the "TipoEquipo" element
     */
    public void xsetTipoEquipo(org.apache.xmlbeans.XmlString tipoEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPOEQUIPO$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TIPOEQUIPO$4);
            }
            target.set(tipoEquipo);
        }
    }
    
    /**
     * Gets the "CantidadPuertos" element
     */
    public java.lang.String getCantidadPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CANTIDADPUERTOS$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CantidadPuertos" element
     */
    public org.apache.xmlbeans.XmlString xgetCantidadPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CANTIDADPUERTOS$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "CantidadPuertos" element
     */
    public void setCantidadPuertos(java.lang.String cantidadPuertos)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CANTIDADPUERTOS$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CANTIDADPUERTOS$6);
            }
            target.setStringValue(cantidadPuertos);
        }
    }
    
    /**
     * Sets (as xml) the "CantidadPuertos" element
     */
    public void xsetCantidadPuertos(org.apache.xmlbeans.XmlString cantidadPuertos)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CANTIDADPUERTOS$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CANTIDADPUERTOS$6);
            }
            target.set(cantidadPuertos);
        }
    }
    
    /**
     * Gets the "DetalleRespuesta" element
     */
    public co.net.une.www.ncainvm6.Detallerespuestatype getDetalleRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Detallerespuestatype target = null;
            target = (co.net.une.www.ncainvm6.Detallerespuestatype)get_store().find_element_user(DETALLERESPUESTA$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "DetalleRespuesta" element
     */
    public boolean isNilDetalleRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Detallerespuestatype target = null;
            target = (co.net.une.www.ncainvm6.Detallerespuestatype)get_store().find_element_user(DETALLERESPUESTA$8, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "DetalleRespuesta" element
     */
    public boolean isSetDetalleRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DETALLERESPUESTA$8) != 0;
        }
    }
    
    /**
     * Sets the "DetalleRespuesta" element
     */
    public void setDetalleRespuesta(co.net.une.www.ncainvm6.Detallerespuestatype detalleRespuesta)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Detallerespuestatype target = null;
            target = (co.net.une.www.ncainvm6.Detallerespuestatype)get_store().find_element_user(DETALLERESPUESTA$8, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Detallerespuestatype)get_store().add_element_user(DETALLERESPUESTA$8);
            }
            target.set(detalleRespuesta);
        }
    }
    
    /**
     * Appends and returns a new empty "DetalleRespuesta" element
     */
    public co.net.une.www.ncainvm6.Detallerespuestatype addNewDetalleRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Detallerespuestatype target = null;
            target = (co.net.une.www.ncainvm6.Detallerespuestatype)get_store().add_element_user(DETALLERESPUESTA$8);
            return target;
        }
    }
    
    /**
     * Nils the "DetalleRespuesta" element
     */
    public void setNilDetalleRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Detallerespuestatype target = null;
            target = (co.net.une.www.ncainvm6.Detallerespuestatype)get_store().find_element_user(DETALLERESPUESTA$8, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Detallerespuestatype)get_store().add_element_user(DETALLERESPUESTA$8);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "DetalleRespuesta" element
     */
    public void unsetDetalleRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DETALLERESPUESTA$8, 0);
        }
    }
    
    /**
     * Gets the "Puertos" element
     */
    public co.net.une.www.ncainvm6.Listapuertotype getPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listapuertotype target = null;
            target = (co.net.une.www.ncainvm6.Listapuertotype)get_store().find_element_user(PUERTOS$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "Puertos" element
     */
    public boolean isNilPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listapuertotype target = null;
            target = (co.net.une.www.ncainvm6.Listapuertotype)get_store().find_element_user(PUERTOS$10, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "Puertos" element
     */
    public boolean isSetPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PUERTOS$10) != 0;
        }
    }
    
    /**
     * Sets the "Puertos" element
     */
    public void setPuertos(co.net.une.www.ncainvm6.Listapuertotype puertos)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listapuertotype target = null;
            target = (co.net.une.www.ncainvm6.Listapuertotype)get_store().find_element_user(PUERTOS$10, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Listapuertotype)get_store().add_element_user(PUERTOS$10);
            }
            target.set(puertos);
        }
    }
    
    /**
     * Appends and returns a new empty "Puertos" element
     */
    public co.net.une.www.ncainvm6.Listapuertotype addNewPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listapuertotype target = null;
            target = (co.net.une.www.ncainvm6.Listapuertotype)get_store().add_element_user(PUERTOS$10);
            return target;
        }
    }
    
    /**
     * Nils the "Puertos" element
     */
    public void setNilPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listapuertotype target = null;
            target = (co.net.une.www.ncainvm6.Listapuertotype)get_store().find_element_user(PUERTOS$10, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Listapuertotype)get_store().add_element_user(PUERTOS$10);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "Puertos" element
     */
    public void unsetPuertos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PUERTOS$10, 0);
        }
    }
    
    /**
     * Gets the "Atributos" element
     */
    public co.net.une.www.ncainvm6.Listaatributostype getAtributos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaatributostype target = null;
            target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().find_element_user(ATRIBUTOS$12, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "Atributos" element
     */
    public boolean isNilAtributos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaatributostype target = null;
            target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().find_element_user(ATRIBUTOS$12, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "Atributos" element
     */
    public boolean isSetAtributos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ATRIBUTOS$12) != 0;
        }
    }
    
    /**
     * Sets the "Atributos" element
     */
    public void setAtributos(co.net.une.www.ncainvm6.Listaatributostype atributos)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaatributostype target = null;
            target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().find_element_user(ATRIBUTOS$12, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().add_element_user(ATRIBUTOS$12);
            }
            target.set(atributos);
        }
    }
    
    /**
     * Appends and returns a new empty "Atributos" element
     */
    public co.net.une.www.ncainvm6.Listaatributostype addNewAtributos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaatributostype target = null;
            target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().add_element_user(ATRIBUTOS$12);
            return target;
        }
    }
    
    /**
     * Nils the "Atributos" element
     */
    public void setNilAtributos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Listaatributostype target = null;
            target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().find_element_user(ATRIBUTOS$12, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Listaatributostype)get_store().add_element_user(ATRIBUTOS$12);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "Atributos" element
     */
    public void unsetAtributos()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ATRIBUTOS$12, 0);
        }
    }
}
