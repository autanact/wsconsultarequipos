/*
 * XML Type:  listacuentasdomiciliariastype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Listacuentasdomiciliariastype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML listacuentasdomiciliariastype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class ListacuentasdomiciliariastypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Listacuentasdomiciliariastype
{
    
    public ListacuentasdomiciliariastypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CUENTADOMICILIARIA$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "CuentaDomiciliaria");
    
    
    /**
     * Gets array of all "CuentaDomiciliaria" elements
     */
    public java.lang.String[] getCuentaDomiciliariaArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CUENTADOMICILIARIA$0, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "CuentaDomiciliaria" element
     */
    public java.lang.String getCuentaDomiciliariaArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CUENTADOMICILIARIA$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "CuentaDomiciliaria" elements
     */
    public org.apache.xmlbeans.XmlString[] xgetCuentaDomiciliariaArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CUENTADOMICILIARIA$0, targetList);
            org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "CuentaDomiciliaria" element
     */
    public org.apache.xmlbeans.XmlString xgetCuentaDomiciliariaArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CUENTADOMICILIARIA$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return (org.apache.xmlbeans.XmlString)target;
        }
    }
    
    /**
     * Returns number of "CuentaDomiciliaria" element
     */
    public int sizeOfCuentaDomiciliariaArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CUENTADOMICILIARIA$0);
        }
    }
    
    /**
     * Sets array of all "CuentaDomiciliaria" element
     */
    public void setCuentaDomiciliariaArray(java.lang.String[] cuentaDomiciliariaArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(cuentaDomiciliariaArray, CUENTADOMICILIARIA$0);
        }
    }
    
    /**
     * Sets ith "CuentaDomiciliaria" element
     */
    public void setCuentaDomiciliariaArray(int i, java.lang.String cuentaDomiciliaria)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CUENTADOMICILIARIA$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(cuentaDomiciliaria);
        }
    }
    
    /**
     * Sets (as xml) array of all "CuentaDomiciliaria" element
     */
    public void xsetCuentaDomiciliariaArray(org.apache.xmlbeans.XmlString[]cuentaDomiciliariaArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(cuentaDomiciliariaArray, CUENTADOMICILIARIA$0);
        }
    }
    
    /**
     * Sets (as xml) ith "CuentaDomiciliaria" element
     */
    public void xsetCuentaDomiciliariaArray(int i, org.apache.xmlbeans.XmlString cuentaDomiciliaria)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CUENTADOMICILIARIA$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(cuentaDomiciliaria);
        }
    }
    
    /**
     * Inserts the value as the ith "CuentaDomiciliaria" element
     */
    public void insertCuentaDomiciliaria(int i, java.lang.String cuentaDomiciliaria)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(CUENTADOMICILIARIA$0, i);
            target.setStringValue(cuentaDomiciliaria);
        }
    }
    
    /**
     * Appends the value as the last "CuentaDomiciliaria" element
     */
    public void addCuentaDomiciliaria(java.lang.String cuentaDomiciliaria)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CUENTADOMICILIARIA$0);
            target.setStringValue(cuentaDomiciliaria);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "CuentaDomiciliaria" element
     */
    public org.apache.xmlbeans.XmlString insertNewCuentaDomiciliaria(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(CUENTADOMICILIARIA$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "CuentaDomiciliaria" element
     */
    public org.apache.xmlbeans.XmlString addNewCuentaDomiciliaria()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CUENTADOMICILIARIA$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "CuentaDomiciliaria" element
     */
    public void removeCuentaDomiciliaria(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CUENTADOMICILIARIA$0, i);
        }
    }
}
