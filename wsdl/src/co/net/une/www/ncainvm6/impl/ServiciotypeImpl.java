/*
 * XML Type:  serviciotype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Serviciotype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML serviciotype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class ServiciotypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Serviciotype
{
    
    public ServiciotypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TIPOSERVICIO$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "TipoServicio");
    private static final javax.xml.namespace.QName CANTIDADSERVICIOS$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "CantidadServicios");
    private static final javax.xml.namespace.QName DETALLESERVICIOS$4 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "DetalleServicios");
    
    
    /**
     * Gets the "TipoServicio" element
     */
    public java.lang.String getTipoServicio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPOSERVICIO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "TipoServicio" element
     */
    public org.apache.xmlbeans.XmlString xgetTipoServicio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPOSERVICIO$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "TipoServicio" element
     */
    public void setTipoServicio(java.lang.String tipoServicio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPOSERVICIO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TIPOSERVICIO$0);
            }
            target.setStringValue(tipoServicio);
        }
    }
    
    /**
     * Sets (as xml) the "TipoServicio" element
     */
    public void xsetTipoServicio(org.apache.xmlbeans.XmlString tipoServicio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPOSERVICIO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TIPOSERVICIO$0);
            }
            target.set(tipoServicio);
        }
    }
    
    /**
     * Gets the "CantidadServicios" element
     */
    public long getCantidadServicios()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CANTIDADSERVICIOS$2, 0);
            if (target == null)
            {
                return 0L;
            }
            return target.getLongValue();
        }
    }
    
    /**
     * Gets (as xml) the "CantidadServicios" element
     */
    public org.apache.xmlbeans.XmlLong xgetCantidadServicios()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlLong target = null;
            target = (org.apache.xmlbeans.XmlLong)get_store().find_element_user(CANTIDADSERVICIOS$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "CantidadServicios" element
     */
    public void setCantidadServicios(long cantidadServicios)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CANTIDADSERVICIOS$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CANTIDADSERVICIOS$2);
            }
            target.setLongValue(cantidadServicios);
        }
    }
    
    /**
     * Sets (as xml) the "CantidadServicios" element
     */
    public void xsetCantidadServicios(org.apache.xmlbeans.XmlLong cantidadServicios)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlLong target = null;
            target = (org.apache.xmlbeans.XmlLong)get_store().find_element_user(CANTIDADSERVICIOS$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlLong)get_store().add_element_user(CANTIDADSERVICIOS$2);
            }
            target.set(cantidadServicios);
        }
    }
    
    /**
     * Gets the "DetalleServicios" element
     */
    public co.net.une.www.ncainvm6.Detalleserviciotype getDetalleServicios()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Detalleserviciotype target = null;
            target = (co.net.une.www.ncainvm6.Detalleserviciotype)get_store().find_element_user(DETALLESERVICIOS$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "DetalleServicios" element
     */
    public void setDetalleServicios(co.net.une.www.ncainvm6.Detalleserviciotype detalleServicios)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Detalleserviciotype target = null;
            target = (co.net.une.www.ncainvm6.Detalleserviciotype)get_store().find_element_user(DETALLESERVICIOS$4, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.Detalleserviciotype)get_store().add_element_user(DETALLESERVICIOS$4);
            }
            target.set(detalleServicios);
        }
    }
    
    /**
     * Appends and returns a new empty "DetalleServicios" element
     */
    public co.net.une.www.ncainvm6.Detalleserviciotype addNewDetalleServicios()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Detalleserviciotype target = null;
            target = (co.net.une.www.ncainvm6.Detalleserviciotype)get_store().add_element_user(DETALLESERVICIOS$4);
            return target;
        }
    }
}
