/*
 * XML Type:  listaatributosequipotype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Listaatributosequipotype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML listaatributosequipotype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class ListaatributosequipotypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Listaatributosequipotype
{
    
    public ListaatributosequipotypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ATRIBUTOEQUIPO$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "AtributoEquipo");
    
    
    /**
     * Gets array of all "AtributoEquipo" elements
     */
    public co.net.une.www.ncainvm6.Atributoequipotype[] getAtributoEquipoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ATRIBUTOEQUIPO$0, targetList);
            co.net.une.www.ncainvm6.Atributoequipotype[] result = new co.net.une.www.ncainvm6.Atributoequipotype[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "AtributoEquipo" element
     */
    public co.net.une.www.ncainvm6.Atributoequipotype getAtributoEquipoArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Atributoequipotype target = null;
            target = (co.net.une.www.ncainvm6.Atributoequipotype)get_store().find_element_user(ATRIBUTOEQUIPO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "AtributoEquipo" element
     */
    public int sizeOfAtributoEquipoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ATRIBUTOEQUIPO$0);
        }
    }
    
    /**
     * Sets array of all "AtributoEquipo" element
     */
    public void setAtributoEquipoArray(co.net.une.www.ncainvm6.Atributoequipotype[] atributoEquipoArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(atributoEquipoArray, ATRIBUTOEQUIPO$0);
        }
    }
    
    /**
     * Sets ith "AtributoEquipo" element
     */
    public void setAtributoEquipoArray(int i, co.net.une.www.ncainvm6.Atributoequipotype atributoEquipo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Atributoequipotype target = null;
            target = (co.net.une.www.ncainvm6.Atributoequipotype)get_store().find_element_user(ATRIBUTOEQUIPO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(atributoEquipo);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "AtributoEquipo" element
     */
    public co.net.une.www.ncainvm6.Atributoequipotype insertNewAtributoEquipo(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Atributoequipotype target = null;
            target = (co.net.une.www.ncainvm6.Atributoequipotype)get_store().insert_element_user(ATRIBUTOEQUIPO$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "AtributoEquipo" element
     */
    public co.net.une.www.ncainvm6.Atributoequipotype addNewAtributoEquipo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Atributoequipotype target = null;
            target = (co.net.une.www.ncainvm6.Atributoequipotype)get_store().add_element_user(ATRIBUTOEQUIPO$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "AtributoEquipo" element
     */
    public void removeAtributoEquipo(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ATRIBUTOEQUIPO$0, i);
        }
    }
}
