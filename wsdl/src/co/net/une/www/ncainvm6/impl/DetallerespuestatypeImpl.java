/*
 * XML Type:  detallerespuestatype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Detallerespuestatype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML detallerespuestatype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class DetallerespuestatypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Detallerespuestatype
{
    
    public DetallerespuestatypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName FECHARESPUESTA$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "FechaRespuesta");
    private static final javax.xml.namespace.QName CODIGORESPUESTA$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "CodigoRespuesta");
    private static final javax.xml.namespace.QName CODIGOERROR$4 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "CodigoError");
    private static final javax.xml.namespace.QName DESCRIPCIONERROR$6 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "DescripcionError");
    private static final javax.xml.namespace.QName MENSAJEERROR$8 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "MensajeError");
    
    
    /**
     * Gets the "FechaRespuesta" element
     */
    public co.net.une.www.ncainvm6.UTCDate getFechaRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.UTCDate target = null;
            target = (co.net.une.www.ncainvm6.UTCDate)get_store().find_element_user(FECHARESPUESTA$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "FechaRespuesta" element
     */
    public boolean isNilFechaRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.UTCDate target = null;
            target = (co.net.une.www.ncainvm6.UTCDate)get_store().find_element_user(FECHARESPUESTA$0, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "FechaRespuesta" element
     */
    public boolean isSetFechaRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FECHARESPUESTA$0) != 0;
        }
    }
    
    /**
     * Sets the "FechaRespuesta" element
     */
    public void setFechaRespuesta(co.net.une.www.ncainvm6.UTCDate fechaRespuesta)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.UTCDate target = null;
            target = (co.net.une.www.ncainvm6.UTCDate)get_store().find_element_user(FECHARESPUESTA$0, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.UTCDate)get_store().add_element_user(FECHARESPUESTA$0);
            }
            target.set(fechaRespuesta);
        }
    }
    
    /**
     * Appends and returns a new empty "FechaRespuesta" element
     */
    public co.net.une.www.ncainvm6.UTCDate addNewFechaRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.UTCDate target = null;
            target = (co.net.une.www.ncainvm6.UTCDate)get_store().add_element_user(FECHARESPUESTA$0);
            return target;
        }
    }
    
    /**
     * Nils the "FechaRespuesta" element
     */
    public void setNilFechaRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.UTCDate target = null;
            target = (co.net.une.www.ncainvm6.UTCDate)get_store().find_element_user(FECHARESPUESTA$0, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.UTCDate)get_store().add_element_user(FECHARESPUESTA$0);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "FechaRespuesta" element
     */
    public void unsetFechaRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FECHARESPUESTA$0, 0);
        }
    }
    
    /**
     * Gets the "CodigoRespuesta" element
     */
    public java.lang.String getCodigoRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODIGORESPUESTA$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CodigoRespuesta" element
     */
    public org.apache.xmlbeans.XmlString xgetCodigoRespuesta()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CODIGORESPUESTA$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "CodigoRespuesta" element
     */
    public void setCodigoRespuesta(java.lang.String codigoRespuesta)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODIGORESPUESTA$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CODIGORESPUESTA$2);
            }
            target.setStringValue(codigoRespuesta);
        }
    }
    
    /**
     * Sets (as xml) the "CodigoRespuesta" element
     */
    public void xsetCodigoRespuesta(org.apache.xmlbeans.XmlString codigoRespuesta)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CODIGORESPUESTA$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CODIGORESPUESTA$2);
            }
            target.set(codigoRespuesta);
        }
    }
    
    /**
     * Gets the "CodigoError" element
     */
    public java.lang.String getCodigoError()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODIGOERROR$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CodigoError" element
     */
    public org.apache.xmlbeans.XmlString xgetCodigoError()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CODIGOERROR$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "CodigoError" element
     */
    public void setCodigoError(java.lang.String codigoError)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODIGOERROR$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CODIGOERROR$4);
            }
            target.setStringValue(codigoError);
        }
    }
    
    /**
     * Sets (as xml) the "CodigoError" element
     */
    public void xsetCodigoError(org.apache.xmlbeans.XmlString codigoError)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CODIGOERROR$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CODIGOERROR$4);
            }
            target.set(codigoError);
        }
    }
    
    /**
     * Gets the "DescripcionError" element
     */
    public java.lang.String getDescripcionError()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPCIONERROR$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "DescripcionError" element
     */
    public org.apache.xmlbeans.XmlString xgetDescripcionError()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPCIONERROR$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "DescripcionError" element
     */
    public void setDescripcionError(java.lang.String descripcionError)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPCIONERROR$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESCRIPCIONERROR$6);
            }
            target.setStringValue(descripcionError);
        }
    }
    
    /**
     * Sets (as xml) the "DescripcionError" element
     */
    public void xsetDescripcionError(org.apache.xmlbeans.XmlString descripcionError)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPCIONERROR$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DESCRIPCIONERROR$6);
            }
            target.set(descripcionError);
        }
    }
    
    /**
     * Gets the "MensajeError" element
     */
    public java.lang.String getMensajeError()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MENSAJEERROR$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MensajeError" element
     */
    public org.apache.xmlbeans.XmlString xgetMensajeError()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MENSAJEERROR$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "MensajeError" element
     */
    public void setMensajeError(java.lang.String mensajeError)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MENSAJEERROR$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MENSAJEERROR$8);
            }
            target.setStringValue(mensajeError);
        }
    }
    
    /**
     * Sets (as xml) the "MensajeError" element
     */
    public void xsetMensajeError(org.apache.xmlbeans.XmlString mensajeError)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MENSAJEERROR$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MENSAJEERROR$8);
            }
            target.set(mensajeError);
        }
    }
}
