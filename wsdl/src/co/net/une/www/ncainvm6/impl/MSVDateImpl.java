/*
 * XML Type:  MSVDate
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.MSVDate
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML MSVDate(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class MSVDateImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.MSVDate
{
    
    public MSVDateImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DATE$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "date");
    
    
    /**
     * Gets the "date" element
     */
    public java.lang.String getDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "date" element
     */
    public co.net.une.www.ncainvm6.BoundedString14 xgetDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.BoundedString14 target = null;
            target = (co.net.une.www.ncainvm6.BoundedString14)get_store().find_element_user(DATE$0, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "date" element
     */
    public boolean isNilDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.BoundedString14 target = null;
            target = (co.net.une.www.ncainvm6.BoundedString14)get_store().find_element_user(DATE$0, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * Sets the "date" element
     */
    public void setDate(java.lang.String date)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DATE$0);
            }
            target.setStringValue(date);
        }
    }
    
    /**
     * Sets (as xml) the "date" element
     */
    public void xsetDate(co.net.une.www.ncainvm6.BoundedString14 date)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.BoundedString14 target = null;
            target = (co.net.une.www.ncainvm6.BoundedString14)get_store().find_element_user(DATE$0, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.BoundedString14)get_store().add_element_user(DATE$0);
            }
            target.set(date);
        }
    }
    
    /**
     * Nils the "date" element
     */
    public void setNilDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.BoundedString14 target = null;
            target = (co.net.une.www.ncainvm6.BoundedString14)get_store().find_element_user(DATE$0, 0);
            if (target == null)
            {
                target = (co.net.une.www.ncainvm6.BoundedString14)get_store().add_element_user(DATE$0);
            }
            target.setNil();
        }
    }
}
