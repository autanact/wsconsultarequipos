/*
 * XML Type:  listapuertotype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Listapuertotype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML listapuertotype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class ListapuertotypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Listapuertotype
{
    
    public ListapuertotypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PUERTO$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Puerto");
    
    
    /**
     * Gets array of all "Puerto" elements
     */
    public co.net.une.www.ncainvm6.Puertostype[] getPuertoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(PUERTO$0, targetList);
            co.net.une.www.ncainvm6.Puertostype[] result = new co.net.une.www.ncainvm6.Puertostype[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Puerto" element
     */
    public co.net.une.www.ncainvm6.Puertostype getPuertoArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Puertostype target = null;
            target = (co.net.une.www.ncainvm6.Puertostype)get_store().find_element_user(PUERTO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Puerto" element
     */
    public int sizeOfPuertoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PUERTO$0);
        }
    }
    
    /**
     * Sets array of all "Puerto" element
     */
    public void setPuertoArray(co.net.une.www.ncainvm6.Puertostype[] puertoArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(puertoArray, PUERTO$0);
        }
    }
    
    /**
     * Sets ith "Puerto" element
     */
    public void setPuertoArray(int i, co.net.une.www.ncainvm6.Puertostype puerto)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Puertostype target = null;
            target = (co.net.une.www.ncainvm6.Puertostype)get_store().find_element_user(PUERTO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(puerto);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Puerto" element
     */
    public co.net.une.www.ncainvm6.Puertostype insertNewPuerto(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Puertostype target = null;
            target = (co.net.une.www.ncainvm6.Puertostype)get_store().insert_element_user(PUERTO$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Puerto" element
     */
    public co.net.une.www.ncainvm6.Puertostype addNewPuerto()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Puertostype target = null;
            target = (co.net.une.www.ncainvm6.Puertostype)get_store().add_element_user(PUERTO$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Puerto" element
     */
    public void removePuerto(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PUERTO$0, i);
        }
    }
}
