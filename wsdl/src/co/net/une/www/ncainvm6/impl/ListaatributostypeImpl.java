/*
 * XML Type:  listaatributostype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Listaatributostype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML listaatributostype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class ListaatributostypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Listaatributostype
{
    
    public ListaatributostypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ATRIBUTO$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "Atributo");
    
    
    /**
     * Gets array of all "Atributo" elements
     */
    public co.net.une.www.ncainvm6.Atributotype[] getAtributoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ATRIBUTO$0, targetList);
            co.net.une.www.ncainvm6.Atributotype[] result = new co.net.une.www.ncainvm6.Atributotype[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Atributo" element
     */
    public co.net.une.www.ncainvm6.Atributotype getAtributoArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Atributotype target = null;
            target = (co.net.une.www.ncainvm6.Atributotype)get_store().find_element_user(ATRIBUTO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Atributo" element
     */
    public int sizeOfAtributoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ATRIBUTO$0);
        }
    }
    
    /**
     * Sets array of all "Atributo" element
     */
    public void setAtributoArray(co.net.une.www.ncainvm6.Atributotype[] atributoArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(atributoArray, ATRIBUTO$0);
        }
    }
    
    /**
     * Sets ith "Atributo" element
     */
    public void setAtributoArray(int i, co.net.une.www.ncainvm6.Atributotype atributo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Atributotype target = null;
            target = (co.net.une.www.ncainvm6.Atributotype)get_store().find_element_user(ATRIBUTO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(atributo);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Atributo" element
     */
    public co.net.une.www.ncainvm6.Atributotype insertNewAtributo(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Atributotype target = null;
            target = (co.net.une.www.ncainvm6.Atributotype)get_store().insert_element_user(ATRIBUTO$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Atributo" element
     */
    public co.net.une.www.ncainvm6.Atributotype addNewAtributo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            co.net.une.www.ncainvm6.Atributotype target = null;
            target = (co.net.une.www.ncainvm6.Atributotype)get_store().add_element_user(ATRIBUTO$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Atributo" element
     */
    public void removeAtributo(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ATRIBUTO$0, i);
        }
    }
}
