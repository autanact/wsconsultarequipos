/*
 * XML Type:  arearedtype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Arearedtype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6.impl;
/**
 * An XML arearedtype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public class ArearedtypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements co.net.une.www.ncainvm6.Arearedtype
{
    
    public ArearedtypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDAREARED$0 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "IdAreaRed");
    private static final javax.xml.namespace.QName NOMBREAREARED$2 = 
        new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6", "NombreAreaRed");
    
    
    /**
     * Gets the "IdAreaRed" element
     */
    public java.lang.String getIdAreaRed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDAREARED$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdAreaRed" element
     */
    public org.apache.xmlbeans.XmlString xgetIdAreaRed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDAREARED$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdAreaRed" element
     */
    public void setIdAreaRed(java.lang.String idAreaRed)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDAREARED$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDAREARED$0);
            }
            target.setStringValue(idAreaRed);
        }
    }
    
    /**
     * Sets (as xml) the "IdAreaRed" element
     */
    public void xsetIdAreaRed(org.apache.xmlbeans.XmlString idAreaRed)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IDAREARED$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IDAREARED$0);
            }
            target.set(idAreaRed);
        }
    }
    
    /**
     * Gets the "NombreAreaRed" element
     */
    public java.lang.String getNombreAreaRed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOMBREAREARED$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "NombreAreaRed" element
     */
    public org.apache.xmlbeans.XmlString xgetNombreAreaRed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NOMBREAREARED$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "NombreAreaRed" element
     */
    public void setNombreAreaRed(java.lang.String nombreAreaRed)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOMBREAREARED$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NOMBREAREARED$2);
            }
            target.setStringValue(nombreAreaRed);
        }
    }
    
    /**
     * Sets (as xml) the "NombreAreaRed" element
     */
    public void xsetNombreAreaRed(org.apache.xmlbeans.XmlString nombreAreaRed)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NOMBREAREARED$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NOMBREAREARED$2);
            }
            target.set(nombreAreaRed);
        }
    }
}
