/*
 * XML Type:  detallerespuestatype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Detallerespuestatype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6;


/**
 * An XML detallerespuestatype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public interface Detallerespuestatype extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Detallerespuestatype.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sAF421E091741FB4D19C06A5B0D6144CC").resolveHandle("detallerespuestatypebc89type");
    
    /**
     * Gets the "FechaRespuesta" element
     */
    co.net.une.www.ncainvm6.UTCDate getFechaRespuesta();
    
    /**
     * Tests for nil "FechaRespuesta" element
     */
    boolean isNilFechaRespuesta();
    
    /**
     * True if has "FechaRespuesta" element
     */
    boolean isSetFechaRespuesta();
    
    /**
     * Sets the "FechaRespuesta" element
     */
    void setFechaRespuesta(co.net.une.www.ncainvm6.UTCDate fechaRespuesta);
    
    /**
     * Appends and returns a new empty "FechaRespuesta" element
     */
    co.net.une.www.ncainvm6.UTCDate addNewFechaRespuesta();
    
    /**
     * Nils the "FechaRespuesta" element
     */
    void setNilFechaRespuesta();
    
    /**
     * Unsets the "FechaRespuesta" element
     */
    void unsetFechaRespuesta();
    
    /**
     * Gets the "CodigoRespuesta" element
     */
    java.lang.String getCodigoRespuesta();
    
    /**
     * Gets (as xml) the "CodigoRespuesta" element
     */
    org.apache.xmlbeans.XmlString xgetCodigoRespuesta();
    
    /**
     * Sets the "CodigoRespuesta" element
     */
    void setCodigoRespuesta(java.lang.String codigoRespuesta);
    
    /**
     * Sets (as xml) the "CodigoRespuesta" element
     */
    void xsetCodigoRespuesta(org.apache.xmlbeans.XmlString codigoRespuesta);
    
    /**
     * Gets the "CodigoError" element
     */
    java.lang.String getCodigoError();
    
    /**
     * Gets (as xml) the "CodigoError" element
     */
    org.apache.xmlbeans.XmlString xgetCodigoError();
    
    /**
     * Sets the "CodigoError" element
     */
    void setCodigoError(java.lang.String codigoError);
    
    /**
     * Sets (as xml) the "CodigoError" element
     */
    void xsetCodigoError(org.apache.xmlbeans.XmlString codigoError);
    
    /**
     * Gets the "DescripcionError" element
     */
    java.lang.String getDescripcionError();
    
    /**
     * Gets (as xml) the "DescripcionError" element
     */
    org.apache.xmlbeans.XmlString xgetDescripcionError();
    
    /**
     * Sets the "DescripcionError" element
     */
    void setDescripcionError(java.lang.String descripcionError);
    
    /**
     * Sets (as xml) the "DescripcionError" element
     */
    void xsetDescripcionError(org.apache.xmlbeans.XmlString descripcionError);
    
    /**
     * Gets the "MensajeError" element
     */
    java.lang.String getMensajeError();
    
    /**
     * Gets (as xml) the "MensajeError" element
     */
    org.apache.xmlbeans.XmlString xgetMensajeError();
    
    /**
     * Sets the "MensajeError" element
     */
    void setMensajeError(java.lang.String mensajeError);
    
    /**
     * Sets (as xml) the "MensajeError" element
     */
    void xsetMensajeError(org.apache.xmlbeans.XmlString mensajeError);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static co.net.une.www.ncainvm6.Detallerespuestatype newInstance() {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.Detallerespuestatype parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.Detallerespuestatype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
