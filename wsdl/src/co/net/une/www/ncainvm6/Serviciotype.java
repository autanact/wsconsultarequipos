/*
 * XML Type:  serviciotype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Serviciotype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6;


/**
 * An XML serviciotype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public interface Serviciotype extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Serviciotype.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sAF421E091741FB4D19C06A5B0D6144CC").resolveHandle("serviciotype4fbctype");
    
    /**
     * Gets the "TipoServicio" element
     */
    java.lang.String getTipoServicio();
    
    /**
     * Gets (as xml) the "TipoServicio" element
     */
    org.apache.xmlbeans.XmlString xgetTipoServicio();
    
    /**
     * Sets the "TipoServicio" element
     */
    void setTipoServicio(java.lang.String tipoServicio);
    
    /**
     * Sets (as xml) the "TipoServicio" element
     */
    void xsetTipoServicio(org.apache.xmlbeans.XmlString tipoServicio);
    
    /**
     * Gets the "CantidadServicios" element
     */
    long getCantidadServicios();
    
    /**
     * Gets (as xml) the "CantidadServicios" element
     */
    org.apache.xmlbeans.XmlLong xgetCantidadServicios();
    
    /**
     * Sets the "CantidadServicios" element
     */
    void setCantidadServicios(long cantidadServicios);
    
    /**
     * Sets (as xml) the "CantidadServicios" element
     */
    void xsetCantidadServicios(org.apache.xmlbeans.XmlLong cantidadServicios);
    
    /**
     * Gets the "DetalleServicios" element
     */
    co.net.une.www.ncainvm6.Detalleserviciotype getDetalleServicios();
    
    /**
     * Sets the "DetalleServicios" element
     */
    void setDetalleServicios(co.net.une.www.ncainvm6.Detalleserviciotype detalleServicios);
    
    /**
     * Appends and returns a new empty "DetalleServicios" element
     */
    co.net.une.www.ncainvm6.Detalleserviciotype addNewDetalleServicios();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static co.net.une.www.ncainvm6.Serviciotype newInstance() {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static co.net.une.www.ncainvm6.Serviciotype parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static co.net.une.www.ncainvm6.Serviciotype parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static co.net.une.www.ncainvm6.Serviciotype parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.Serviciotype parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.Serviciotype parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.Serviciotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
