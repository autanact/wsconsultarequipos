/*
 * XML Type:  listacuentasdomiciliariastype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Listacuentasdomiciliariastype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6;


/**
 * An XML listacuentasdomiciliariastype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public interface Listacuentasdomiciliariastype extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Listacuentasdomiciliariastype.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sAF421E091741FB4D19C06A5B0D6144CC").resolveHandle("listacuentasdomiciliariastyped70ctype");
    
    /**
     * Gets array of all "CuentaDomiciliaria" elements
     */
    java.lang.String[] getCuentaDomiciliariaArray();
    
    /**
     * Gets ith "CuentaDomiciliaria" element
     */
    java.lang.String getCuentaDomiciliariaArray(int i);
    
    /**
     * Gets (as xml) array of all "CuentaDomiciliaria" elements
     */
    org.apache.xmlbeans.XmlString[] xgetCuentaDomiciliariaArray();
    
    /**
     * Gets (as xml) ith "CuentaDomiciliaria" element
     */
    org.apache.xmlbeans.XmlString xgetCuentaDomiciliariaArray(int i);
    
    /**
     * Returns number of "CuentaDomiciliaria" element
     */
    int sizeOfCuentaDomiciliariaArray();
    
    /**
     * Sets array of all "CuentaDomiciliaria" element
     */
    void setCuentaDomiciliariaArray(java.lang.String[] cuentaDomiciliariaArray);
    
    /**
     * Sets ith "CuentaDomiciliaria" element
     */
    void setCuentaDomiciliariaArray(int i, java.lang.String cuentaDomiciliaria);
    
    /**
     * Sets (as xml) array of all "CuentaDomiciliaria" element
     */
    void xsetCuentaDomiciliariaArray(org.apache.xmlbeans.XmlString[] cuentaDomiciliariaArray);
    
    /**
     * Sets (as xml) ith "CuentaDomiciliaria" element
     */
    void xsetCuentaDomiciliariaArray(int i, org.apache.xmlbeans.XmlString cuentaDomiciliaria);
    
    /**
     * Inserts the value as the ith "CuentaDomiciliaria" element
     */
    void insertCuentaDomiciliaria(int i, java.lang.String cuentaDomiciliaria);
    
    /**
     * Appends the value as the last "CuentaDomiciliaria" element
     */
    void addCuentaDomiciliaria(java.lang.String cuentaDomiciliaria);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "CuentaDomiciliaria" element
     */
    org.apache.xmlbeans.XmlString insertNewCuentaDomiciliaria(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "CuentaDomiciliaria" element
     */
    org.apache.xmlbeans.XmlString addNewCuentaDomiciliaria();
    
    /**
     * Removes the ith "CuentaDomiciliaria" element
     */
    void removeCuentaDomiciliaria(int i);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype newInstance() {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.Listacuentasdomiciliariastype parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.Listacuentasdomiciliariastype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
