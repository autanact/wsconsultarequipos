/*
 * XML Type:  equipotype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.Equipotype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6;


/**
 * An XML equipotype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public interface Equipotype extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Equipotype.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sAF421E091741FB4D19C06A5B0D6144CC").resolveHandle("equipotyped373type");
    
    /**
     * Gets the "Secuencia" element
     */
    java.lang.String getSecuencia();
    
    /**
     * Gets (as xml) the "Secuencia" element
     */
    org.apache.xmlbeans.XmlString xgetSecuencia();
    
    /**
     * Tests for nil "Secuencia" element
     */
    boolean isNilSecuencia();
    
    /**
     * True if has "Secuencia" element
     */
    boolean isSetSecuencia();
    
    /**
     * Sets the "Secuencia" element
     */
    void setSecuencia(java.lang.String secuencia);
    
    /**
     * Sets (as xml) the "Secuencia" element
     */
    void xsetSecuencia(org.apache.xmlbeans.XmlString secuencia);
    
    /**
     * Nils the "Secuencia" element
     */
    void setNilSecuencia();
    
    /**
     * Unsets the "Secuencia" element
     */
    void unsetSecuencia();
    
    /**
     * Gets the "IdLocalizacion" element
     */
    java.lang.String getIdLocalizacion();
    
    /**
     * Gets (as xml) the "IdLocalizacion" element
     */
    org.apache.xmlbeans.XmlString xgetIdLocalizacion();
    
    /**
     * Sets the "IdLocalizacion" element
     */
    void setIdLocalizacion(java.lang.String idLocalizacion);
    
    /**
     * Sets (as xml) the "IdLocalizacion" element
     */
    void xsetIdLocalizacion(org.apache.xmlbeans.XmlString idLocalizacion);
    
    /**
     * Gets the "IdUneEquipoPadre" element
     */
    java.lang.String getIdUneEquipoPadre();
    
    /**
     * Gets (as xml) the "IdUneEquipoPadre" element
     */
    org.apache.xmlbeans.XmlString xgetIdUneEquipoPadre();
    
    /**
     * Sets the "IdUneEquipoPadre" element
     */
    void setIdUneEquipoPadre(java.lang.String idUneEquipoPadre);
    
    /**
     * Sets (as xml) the "IdUneEquipoPadre" element
     */
    void xsetIdUneEquipoPadre(org.apache.xmlbeans.XmlString idUneEquipoPadre);
    
    /**
     * Gets the "IdUneEquipo" element
     */
    java.lang.String getIdUneEquipo();
    
    /**
     * Gets (as xml) the "IdUneEquipo" element
     */
    org.apache.xmlbeans.XmlString xgetIdUneEquipo();
    
    /**
     * Sets the "IdUneEquipo" element
     */
    void setIdUneEquipo(java.lang.String idUneEquipo);
    
    /**
     * Sets (as xml) the "IdUneEquipo" element
     */
    void xsetIdUneEquipo(org.apache.xmlbeans.XmlString idUneEquipo);
    
    /**
     * Gets the "AliasEquipo" element
     */
    java.lang.String getAliasEquipo();
    
    /**
     * Gets (as xml) the "AliasEquipo" element
     */
    org.apache.xmlbeans.XmlString xgetAliasEquipo();
    
    /**
     * Sets the "AliasEquipo" element
     */
    void setAliasEquipo(java.lang.String aliasEquipo);
    
    /**
     * Sets (as xml) the "AliasEquipo" element
     */
    void xsetAliasEquipo(org.apache.xmlbeans.XmlString aliasEquipo);
    
    /**
     * Gets the "TipoEquipo" element
     */
    java.lang.String getTipoEquipo();
    
    /**
     * Gets (as xml) the "TipoEquipo" element
     */
    org.apache.xmlbeans.XmlString xgetTipoEquipo();
    
    /**
     * Sets the "TipoEquipo" element
     */
    void setTipoEquipo(java.lang.String tipoEquipo);
    
    /**
     * Sets (as xml) the "TipoEquipo" element
     */
    void xsetTipoEquipo(org.apache.xmlbeans.XmlString tipoEquipo);
    
    /**
     * Gets the "DescripcionEquipo" element
     */
    java.lang.String getDescripcionEquipo();
    
    /**
     * Gets (as xml) the "DescripcionEquipo" element
     */
    org.apache.xmlbeans.XmlString xgetDescripcionEquipo();
    
    /**
     * Tests for nil "DescripcionEquipo" element
     */
    boolean isNilDescripcionEquipo();
    
    /**
     * True if has "DescripcionEquipo" element
     */
    boolean isSetDescripcionEquipo();
    
    /**
     * Sets the "DescripcionEquipo" element
     */
    void setDescripcionEquipo(java.lang.String descripcionEquipo);
    
    /**
     * Sets (as xml) the "DescripcionEquipo" element
     */
    void xsetDescripcionEquipo(org.apache.xmlbeans.XmlString descripcionEquipo);
    
    /**
     * Nils the "DescripcionEquipo" element
     */
    void setNilDescripcionEquipo();
    
    /**
     * Unsets the "DescripcionEquipo" element
     */
    void unsetDescripcionEquipo();
    
    /**
     * Gets the "CanalEquipo" element
     */
    java.lang.String getCanalEquipo();
    
    /**
     * Gets (as xml) the "CanalEquipo" element
     */
    org.apache.xmlbeans.XmlString xgetCanalEquipo();
    
    /**
     * Tests for nil "CanalEquipo" element
     */
    boolean isNilCanalEquipo();
    
    /**
     * True if has "CanalEquipo" element
     */
    boolean isSetCanalEquipo();
    
    /**
     * Sets the "CanalEquipo" element
     */
    void setCanalEquipo(java.lang.String canalEquipo);
    
    /**
     * Sets (as xml) the "CanalEquipo" element
     */
    void xsetCanalEquipo(org.apache.xmlbeans.XmlString canalEquipo);
    
    /**
     * Nils the "CanalEquipo" element
     */
    void setNilCanalEquipo();
    
    /**
     * Unsets the "CanalEquipo" element
     */
    void unsetCanalEquipo();
    
    /**
     * Gets the "Tecnologia" element
     */
    java.lang.String getTecnologia();
    
    /**
     * Gets (as xml) the "Tecnologia" element
     */
    org.apache.xmlbeans.XmlString xgetTecnologia();
    
    /**
     * Tests for nil "Tecnologia" element
     */
    boolean isNilTecnologia();
    
    /**
     * True if has "Tecnologia" element
     */
    boolean isSetTecnologia();
    
    /**
     * Sets the "Tecnologia" element
     */
    void setTecnologia(java.lang.String tecnologia);
    
    /**
     * Sets (as xml) the "Tecnologia" element
     */
    void xsetTecnologia(org.apache.xmlbeans.XmlString tecnologia);
    
    /**
     * Nils the "Tecnologia" element
     */
    void setNilTecnologia();
    
    /**
     * Unsets the "Tecnologia" element
     */
    void unsetTecnologia();
    
    /**
     * Gets the "UmbralFalla" element
     */
    java.lang.String getUmbralFalla();
    
    /**
     * Gets (as xml) the "UmbralFalla" element
     */
    org.apache.xmlbeans.XmlString xgetUmbralFalla();
    
    /**
     * Tests for nil "UmbralFalla" element
     */
    boolean isNilUmbralFalla();
    
    /**
     * True if has "UmbralFalla" element
     */
    boolean isSetUmbralFalla();
    
    /**
     * Sets the "UmbralFalla" element
     */
    void setUmbralFalla(java.lang.String umbralFalla);
    
    /**
     * Sets (as xml) the "UmbralFalla" element
     */
    void xsetUmbralFalla(org.apache.xmlbeans.XmlString umbralFalla);
    
    /**
     * Nils the "UmbralFalla" element
     */
    void setNilUmbralFalla();
    
    /**
     * Unsets the "UmbralFalla" element
     */
    void unsetUmbralFalla();
    
    /**
     * Gets the "Relevante" element
     */
    java.lang.String getRelevante();
    
    /**
     * Gets (as xml) the "Relevante" element
     */
    org.apache.xmlbeans.XmlString xgetRelevante();
    
    /**
     * Tests for nil "Relevante" element
     */
    boolean isNilRelevante();
    
    /**
     * True if has "Relevante" element
     */
    boolean isSetRelevante();
    
    /**
     * Sets the "Relevante" element
     */
    void setRelevante(java.lang.String relevante);
    
    /**
     * Sets (as xml) the "Relevante" element
     */
    void xsetRelevante(org.apache.xmlbeans.XmlString relevante);
    
    /**
     * Nils the "Relevante" element
     */
    void setNilRelevante();
    
    /**
     * Unsets the "Relevante" element
     */
    void unsetRelevante();
    
    /**
     * Gets the "DireccionElementoRed" element
     */
    java.lang.String getDireccionElementoRed();
    
    /**
     * Gets (as xml) the "DireccionElementoRed" element
     */
    org.apache.xmlbeans.XmlString xgetDireccionElementoRed();
    
    /**
     * Tests for nil "DireccionElementoRed" element
     */
    boolean isNilDireccionElementoRed();
    
    /**
     * True if has "DireccionElementoRed" element
     */
    boolean isSetDireccionElementoRed();
    
    /**
     * Sets the "DireccionElementoRed" element
     */
    void setDireccionElementoRed(java.lang.String direccionElementoRed);
    
    /**
     * Sets (as xml) the "DireccionElementoRed" element
     */
    void xsetDireccionElementoRed(org.apache.xmlbeans.XmlString direccionElementoRed);
    
    /**
     * Nils the "DireccionElementoRed" element
     */
    void setNilDireccionElementoRed();
    
    /**
     * Unsets the "DireccionElementoRed" element
     */
    void unsetDireccionElementoRed();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static co.net.une.www.ncainvm6.Equipotype newInstance() {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static co.net.une.www.ncainvm6.Equipotype newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static co.net.une.www.ncainvm6.Equipotype parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static co.net.une.www.ncainvm6.Equipotype parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static co.net.une.www.ncainvm6.Equipotype parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static co.net.une.www.ncainvm6.Equipotype parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static co.net.une.www.ncainvm6.Equipotype parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static co.net.une.www.ncainvm6.Equipotype parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static co.net.une.www.ncainvm6.Equipotype parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static co.net.une.www.ncainvm6.Equipotype parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static co.net.une.www.ncainvm6.Equipotype parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static co.net.une.www.ncainvm6.Equipotype parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static co.net.une.www.ncainvm6.Equipotype parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static co.net.une.www.ncainvm6.Equipotype parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static co.net.une.www.ncainvm6.Equipotype parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static co.net.une.www.ncainvm6.Equipotype parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.Equipotype parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.Equipotype parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.Equipotype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
