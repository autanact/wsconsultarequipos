/*
 * XML Type:  WsM6ConsultarEquipo-RQ-Type
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6;


/**
 * An XML WsM6ConsultarEquipo-RQ-Type(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public interface WsM6ConsultarEquipoRQType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(WsM6ConsultarEquipoRQType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sAF421E091741FB4D19C06A5B0D6144CC").resolveHandle("wsm6consultarequiporqtype57ectype");
    
    /**
     * Gets the "FechaSolicitud" element
     */
    co.net.une.www.ncainvm6.UTCDate getFechaSolicitud();
    
    /**
     * Sets the "FechaSolicitud" element
     */
    void setFechaSolicitud(co.net.une.www.ncainvm6.UTCDate fechaSolicitud);
    
    /**
     * Appends and returns a new empty "FechaSolicitud" element
     */
    co.net.une.www.ncainvm6.UTCDate addNewFechaSolicitud();
    
    /**
     * Gets the "IdUneEquipo" element
     */
    java.lang.String getIdUneEquipo();
    
    /**
     * Gets (as xml) the "IdUneEquipo" element
     */
    org.apache.xmlbeans.XmlString xgetIdUneEquipo();
    
    /**
     * Sets the "IdUneEquipo" element
     */
    void setIdUneEquipo(java.lang.String idUneEquipo);
    
    /**
     * Sets (as xml) the "IdUneEquipo" element
     */
    void xsetIdUneEquipo(org.apache.xmlbeans.XmlString idUneEquipo);
    
    /**
     * Gets the "TipoEquipo" element
     */
    java.lang.String getTipoEquipo();
    
    /**
     * Gets (as xml) the "TipoEquipo" element
     */
    org.apache.xmlbeans.XmlString xgetTipoEquipo();
    
    /**
     * Sets the "TipoEquipo" element
     */
    void setTipoEquipo(java.lang.String tipoEquipo);
    
    /**
     * Sets (as xml) the "TipoEquipo" element
     */
    void xsetTipoEquipo(org.apache.xmlbeans.XmlString tipoEquipo);
    
    /**
     * Gets the "Atributos" element
     */
    co.net.une.www.ncainvm6.Listaatributostype getAtributos();
    
    /**
     * Tests for nil "Atributos" element
     */
    boolean isNilAtributos();
    
    /**
     * True if has "Atributos" element
     */
    boolean isSetAtributos();
    
    /**
     * Sets the "Atributos" element
     */
    void setAtributos(co.net.une.www.ncainvm6.Listaatributostype atributos);
    
    /**
     * Appends and returns a new empty "Atributos" element
     */
    co.net.une.www.ncainvm6.Listaatributostype addNewAtributos();
    
    /**
     * Nils the "Atributos" element
     */
    void setNilAtributos();
    
    /**
     * Unsets the "Atributos" element
     */
    void unsetAtributos();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType newInstance() {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRQType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
