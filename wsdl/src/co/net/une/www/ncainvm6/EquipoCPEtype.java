/*
 * XML Type:  equipoCPEtype
 * Namespace: http://www.une.net.co/ncaInvM6
 * Java type: co.net.une.www.ncainvm6.EquipoCPEtype
 *
 * Automatically generated - do not modify.
 */
package co.net.une.www.ncainvm6;


/**
 * An XML equipoCPEtype(@http://www.une.net.co/ncaInvM6).
 *
 * This is a complex type.
 */
public interface EquipoCPEtype extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(EquipoCPEtype.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sAF421E091741FB4D19C06A5B0D6144CC").resolveHandle("equipocpetype61cbtype");
    
    /**
     * Gets the "Marca" element
     */
    java.lang.String getMarca();
    
    /**
     * Gets (as xml) the "Marca" element
     */
    org.apache.xmlbeans.XmlString xgetMarca();
    
    /**
     * Sets the "Marca" element
     */
    void setMarca(java.lang.String marca);
    
    /**
     * Sets (as xml) the "Marca" element
     */
    void xsetMarca(org.apache.xmlbeans.XmlString marca);
    
    /**
     * Gets the "Modelo" element
     */
    java.lang.String getModelo();
    
    /**
     * Gets (as xml) the "Modelo" element
     */
    org.apache.xmlbeans.XmlString xgetModelo();
    
    /**
     * Sets the "Modelo" element
     */
    void setModelo(java.lang.String modelo);
    
    /**
     * Sets (as xml) the "Modelo" element
     */
    void xsetModelo(org.apache.xmlbeans.XmlString modelo);
    
    /**
     * Gets the "Serial" element
     */
    java.lang.String getSerial();
    
    /**
     * Gets (as xml) the "Serial" element
     */
    org.apache.xmlbeans.XmlString xgetSerial();
    
    /**
     * Sets the "Serial" element
     */
    void setSerial(java.lang.String serial);
    
    /**
     * Sets (as xml) the "Serial" element
     */
    void xsetSerial(org.apache.xmlbeans.XmlString serial);
    
    /**
     * Gets the "Estado" element
     */
    java.lang.String getEstado();
    
    /**
     * Gets (as xml) the "Estado" element
     */
    org.apache.xmlbeans.XmlString xgetEstado();
    
    /**
     * Sets the "Estado" element
     */
    void setEstado(java.lang.String estado);
    
    /**
     * Sets (as xml) the "Estado" element
     */
    void xsetEstado(org.apache.xmlbeans.XmlString estado);
    
    /**
     * Gets the "Tipo" element
     */
    java.lang.String getTipo();
    
    /**
     * Gets (as xml) the "Tipo" element
     */
    org.apache.xmlbeans.XmlString xgetTipo();
    
    /**
     * Sets the "Tipo" element
     */
    void setTipo(java.lang.String tipo);
    
    /**
     * Sets (as xml) the "Tipo" element
     */
    void xsetTipo(org.apache.xmlbeans.XmlString tipo);
    
    /**
     * Gets the "IdSap" element
     */
    java.lang.String getIdSap();
    
    /**
     * Gets (as xml) the "IdSap" element
     */
    org.apache.xmlbeans.XmlString xgetIdSap();
    
    /**
     * Sets the "IdSap" element
     */
    void setIdSap(java.lang.String idSap);
    
    /**
     * Sets (as xml) the "IdSap" element
     */
    void xsetIdSap(org.apache.xmlbeans.XmlString idSap);
    
    /**
     * Gets the "MAC1" element
     */
    java.lang.String getMAC1();
    
    /**
     * Gets (as xml) the "MAC1" element
     */
    org.apache.xmlbeans.XmlString xgetMAC1();
    
    /**
     * Tests for nil "MAC1" element
     */
    boolean isNilMAC1();
    
    /**
     * True if has "MAC1" element
     */
    boolean isSetMAC1();
    
    /**
     * Sets the "MAC1" element
     */
    void setMAC1(java.lang.String mac1);
    
    /**
     * Sets (as xml) the "MAC1" element
     */
    void xsetMAC1(org.apache.xmlbeans.XmlString mac1);
    
    /**
     * Nils the "MAC1" element
     */
    void setNilMAC1();
    
    /**
     * Unsets the "MAC1" element
     */
    void unsetMAC1();
    
    /**
     * Gets the "MAC2" element
     */
    java.lang.String getMAC2();
    
    /**
     * Gets (as xml) the "MAC2" element
     */
    org.apache.xmlbeans.XmlString xgetMAC2();
    
    /**
     * Tests for nil "MAC2" element
     */
    boolean isNilMAC2();
    
    /**
     * True if has "MAC2" element
     */
    boolean isSetMAC2();
    
    /**
     * Sets the "MAC2" element
     */
    void setMAC2(java.lang.String mac2);
    
    /**
     * Sets (as xml) the "MAC2" element
     */
    void xsetMAC2(org.apache.xmlbeans.XmlString mac2);
    
    /**
     * Nils the "MAC2" element
     */
    void setNilMAC2();
    
    /**
     * Unsets the "MAC2" element
     */
    void unsetMAC2();
    
    /**
     * Gets the "TipoDeco" element
     */
    java.lang.String getTipoDeco();
    
    /**
     * Gets (as xml) the "TipoDeco" element
     */
    org.apache.xmlbeans.XmlString xgetTipoDeco();
    
    /**
     * Sets the "TipoDeco" element
     */
    void setTipoDeco(java.lang.String tipoDeco);
    
    /**
     * Sets (as xml) the "TipoDeco" element
     */
    void xsetTipoDeco(org.apache.xmlbeans.XmlString tipoDeco);
    
    /**
     * Gets the "TipoModem" element
     */
    java.lang.String getTipoModem();
    
    /**
     * Gets (as xml) the "TipoModem" element
     */
    org.apache.xmlbeans.XmlString xgetTipoModem();
    
    /**
     * Sets the "TipoModem" element
     */
    void setTipoModem(java.lang.String tipoModem);
    
    /**
     * Sets (as xml) the "TipoModem" element
     */
    void xsetTipoModem(org.apache.xmlbeans.XmlString tipoModem);
    
    /**
     * Gets the "Puertos" element
     */
    co.net.une.www.ncainvm6.Listapuertostype getPuertos();
    
    /**
     * Tests for nil "Puertos" element
     */
    boolean isNilPuertos();
    
    /**
     * True if has "Puertos" element
     */
    boolean isSetPuertos();
    
    /**
     * Sets the "Puertos" element
     */
    void setPuertos(co.net.une.www.ncainvm6.Listapuertostype puertos);
    
    /**
     * Appends and returns a new empty "Puertos" element
     */
    co.net.une.www.ncainvm6.Listapuertostype addNewPuertos();
    
    /**
     * Nils the "Puertos" element
     */
    void setNilPuertos();
    
    /**
     * Unsets the "Puertos" element
     */
    void unsetPuertos();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static co.net.une.www.ncainvm6.EquipoCPEtype newInstance() {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static co.net.une.www.ncainvm6.EquipoCPEtype parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (co.net.une.www.ncainvm6.EquipoCPEtype) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
