package co.net.une.www.svcEJB;

/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Servicios WEB GSS SmallWorld 4.3
##	Archivo: WsM6ConsultarEquipoServiceBean.java
##	Contenido: Clase que contiene la implementaci�n del servicio WsM6ConsultarEquipoService
##	Autor: Freddy Molina
##  Fecha creaci�n: 19-02-2016
##	Fecha �ltima modificaci�n: 19-02-2016
##	Historial de cambios: 
##	19-02-2016	FJM		Primera versi�n
##
##**********************************************************************************************************************************
*/

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import co.net.une.ejb43.util.ServiciosUtil;
import co.net.une.www.ncainvm6.Atributotype;
import co.net.une.www.ncainvm6.BoundedString14;
import co.net.une.www.ncainvm6.Detallerespuestatype;
import co.net.une.www.ncainvm6.Listaatributostype;
import co.net.une.www.ncainvm6.Listapuertotype;
import co.net.une.www.ncainvm6.Puertostype;
import co.net.une.www.ncainvm6.UTCDate;
import co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType;

import com.gesmallworld.gss.lib.service.ServiceLocal;
import com.gesmallworld.gss.lib.service.magik.MagikService;
import com.gesmallworld.gss.lib.auth.AuthorisationException;
import com.gesmallworld.gss.lib.exception.GSSException;
import com.gesmallworld.gss.lib.locator.ServiceLocator;
import com.gesmallworld.gss.lib.locator.ServiceLocatorException;
import com.gesmallworld.gss.lib.log.SWLog;
import com.gesmallworld.gss.lib.request.BusinessResponse;
import com.gesmallworld.gss.lib.request.ParameterException;
import com.gesmallworld.gss.lib.request.Request;

import javax.ejb.SessionBean;

import org.apache.log4j.Level;

import com.gesmallworld.gss.lib.request.Response;
import com.gesmallworld.gss.lib.service.magik.MagikService.StateHandling;

/**
 * Clase que contiene la implementaci�n del servicio WsM6ConsultarEquipoService
 * @author FreddyMolina
 * @creado 19/02/2016
 * @ultimamodificacion 19/02/2016
 * @version 1.0
 * @historial
 * 			19/02/2016 FJM Primera versi�n
 * 
 * @ejb.resource-ref res-type="javax.resource.cci.ConnectionFactory"
 *                   res-auth="Container" res-ref-name="eis/SmallworldServer"
 *                   jndi-name="eis/SmallworldServer"
 *                   res-sharing-scope="Shareable"
 * @ejb.interface local-extends=
 *                "javax.ejb.EJBLocalObject, com.gesmallworld.gss.lib.service.ChainableServiceLocal"
 *                package="co.net.une.www.interfaces"
 * @ejb.bean view-type="local" name="WsM6ConsultarEquipoService"
 *           type="Stateless"
 *           local-jndi-name="ejb/WsM6ConsultarEquipoServiceLocal"
 *           transaction-type="Bean"
 * @ejb.home local-extends="javax.ejb.EJBLocalHome"
 *           package="co.net.une.www.interfaces"
 */
@StateHandling(serviceProvider = "ws_m6_consultar_equipo_service_service_provider")
public class WsM6ConsultarEquipoServiceBean extends MagikService implements
		SessionBean {

	/**
	 * Local jndi name in use for this service bean.
	 */
	public static final String LOCAL_JNDI_NAME = "ejb/WsM6ConsultarEquipoServiceLocal";

	/**
	 * Serialisation ID.
	 */
	private static final long serialVersionUID = 1L;
	

	/**
	 * Mensajes en properties
	 */
	//archivo de propiedades
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.WsConsultarEquipos", Locale.getDefault());

	//validaci�n gen�rica de errores GSS
	private static final String KeyGSSException = "validate.GSSException.code";
	//validaci�n gen�rica de errores
	private static final String KeyGeneralException = "validate.generalException.code";
	//validaci�n de par�metros repetidos
    private static final String KeyRequiredParameterException = "validate.required.parameterException.code";
    //validaci�n tipo de equipo valido
    private static final String keyTipoEquipoException = "validate.tipoEquipo";
    //mensaje de error de par�metro inv�lido
	private String mensajeErrorParametroInvalido = "";
	
    //EJB encadenado
    private static final String KeyChainedEJB =  rb.getString("parameter.chainedEJB");
    //funci�n EJB a ejecutar
    private static final String KeyChainedEJBFunction = rb.getString("parameter.chainedEJB.function");
    //nombre del servicio
    private static final String keyNombreServicio = rb.getString("servicio.nombre");
    //nombre de la funci�n
    private static final String keyNombreFuncion = rb.getString("servicio.nombrefuncion");
    //tipo de equipo valido
    private static final String keyTipoEquipo = rb.getString("validate.tipoEquipoValido");

	/**
	 * Constructor.
	 */
	public WsM6ConsultarEquipoServiceBean() {
		super();
	}

	/**
	 * Generated service proxy method. Corresponds to a service of a Magik
	 * Service Provider.
	 * 
	 * @ejb.interface-method
	 * @param request
	 *            A request object as specified in the service description file.
	 * @return A response instance
	 */
	@SuppressWarnings("unchecked")
	@EISMapping(value = "consultar_equipo")
	public Response consultarEquipo(Request request) {
		
		//se configuran los par�metros de salida
		WsM6ConsultarEquipoRSType consultaEquipoRSType = new WsM6ConsultarEquipoRSType();
		
		// inicializo los par�metros de respuesta
		inicializarParametrosSalida(consultaEquipoRSType);
		
		// Defino puente entre request y response
		Response response = new BusinessResponse(request);
		
		try {
		
			//Obtengo los parametros de entrada del servicio
			//obtengo la fecha de la solicitud
			UTCDate fechaSolicitud = (UTCDate) request.getParameter("fechaSolicitud");
			//obtengo IdUneEquipo
			String idUneEquipo = (String) request.getParameter("idUneEquipo");
			//obtengo tipoEquipo
			String tipoEquipo = (String) request.getParameter("tipoEquipo");
			//obtengo lista de atributos
			Listaatributostype listaAtributos = (Listaatributostype)  request.getParameter("atributos");
			
			//obtengo el par�metro IdUneEquipo es el mismo de la entrada
			consultaEquipoRSType.setIdUneEquipo(idUneEquipo);
			//obtengo el par�metro TipoEquipo es el mismo de la entrada
			consultaEquipoRSType.setTipoEquipo(tipoEquipo);
			//la fecha de solicitud de la salida debe ser la misma que la de la entrada
			consultaEquipoRSType.setFechaSolicitud(fechaSolicitud);	
			
			//valido los par�metros de entrada
			//FechaSolicitud es obligatorio
			validarCampoObligatorio("FechaSolicitud", fechaSolicitud, consultaEquipoRSType.getDetalleRespuesta());		
			//IdUneEquipo es obligatorio
			validarCampoObligatorio("IdUneEquipo", idUneEquipo, consultaEquipoRSType.getDetalleRespuesta());
			//TipoEquipo es obligatorio
			validarCampoObligatorio("TipoEquipo", tipoEquipo, consultaEquipoRSType.getDetalleRespuesta());
			//valido que el tipo de equipo sea solo TAP
			if (!keyTipoEquipo.equals(tipoEquipo)){
				//el campo es diferente, lanzo excepcion
				registrarException(consultaEquipoRSType.getDetalleRespuesta(), rb.getString(keyTipoEquipoException + ".code") ,MessageFormat.format(rb.getString(keyTipoEquipoException), tipoEquipo), null, false);
				lanzarParameterException();
			}

			
			//obtengo el listado de atributos si est�n presentes
			Listaatributostype listaAtribSalida =null;
			if (listaAtributos!=null && !"".equals(listaAtributos)) {
				//recorro los atributos
				listaAtribSalida = new Listaatributostype();
				//por cada atributo de la lista
				boolean ingrese=false;
				for(Atributotype atributo:listaAtributos.getAtributo()){
					if(!ServiciosUtil.esCampoVacio(atributo.getIdAtributo()) && !ServiciosUtil.esCampoVacio(atributo.getIdAtributoPadre())
							&& !ServiciosUtil.esCampoVacio(atributo.getNombreAtributo()) && !ServiciosUtil.esCampoVacio(atributo.getValorAtributo())){
						listaAtribSalida.addAtributo(atributo);
						ingrese= true;
					}
				}
				if (!ingrese)
					listaAtribSalida = null;
			}
			
			// Encadeno la llamada al Magik
			Request r = request.createChainedRequest(KeyChainedEJB, KeyChainedEJBFunction);	
			
			// Asigno el parametro de entrada al servicio Magik
			HashMap<String, Object> parametros = new HashMap<String, Object>();
			//ingreso idUneEquipo
			parametros.put("idUneEquipo", idUneEquipo);
			//ingreso tipoEquipo
			parametros.put("tipoEquipo",tipoEquipo);
			
			r.setParameter("parametros", parametros);
			
			//Ejecuto el servicio Magik
			ServiceLocal s = (ServiceLocal) ServiceLocator.getInstance().getSLSB(KeyChainedEJB);
			Response gssServiceResponse = s.makeRequest(r);

			//Obtengo la respuesta del servicio Magik
			Map<String, Object> respuestaMagik = gssServiceResponse.getResponses();
			
			HashMap<String, Object> respuesta = (HashMap<String, Object>) respuestaMagik.get("respuesta");
			
			//Obtengo y asigno los puertos
			//HashMap<String, Object> puertos = (HashMap<String, Object>) respuesta.get("puertos");
			
			Object[] arrayPuertos = (Object[]) respuesta.get("puertos");
			int cantPuertos =0;
			if (arrayPuertos!=null  && arrayPuertos.length >0){
				//si hay puertos los analizo
				Listapuertotype puertosSalida = new Listapuertotype();	
				//obtengo cada uno de los puertos encontrados
				for ( int i=0; i < arrayPuertos.length;i++ ){	
					
					HashMap<String, String> puertosHast = (HashMap<String, String>) arrayPuertos[i];
					
					//obtengo cada uno de los parametros del puerto
					Iterator<String> keyMapIterator = puertosHast.keySet().iterator();
					Puertostype puerto = new Puertostype();
					
					//ingreso al puerto los parametros y valores obtenidos
					while(keyMapIterator.hasNext()){
						String key2 = keyMapIterator.next();
						//obtengo el NroPuerto
						if ("NroPuerto".equals(key2)){
							puerto.setNroPuerto(puertosHast.get(key2));
						}
						//obtengo el Estado
						if ("Estado".equals(key2)){
							puerto.setEstado(puertosHast.get(key2));
						}
						//obtengo el RFS
						if ("RFS".equals(key2)){
							puerto.setRFS(puertosHast.get(key2));
						}	
						//obtengo el IdDireccion
						if ("IdDireccion".equals(key2)){
							puerto.setIdDireccion(puertosHast.get(key2));
						}
						
					}
					
					cantPuertos++;
					//ingreso el puerto a la lista de puertos
					puertosSalida.addPuerto(puerto);
					
				}
				
				//ingreso la lista a los puertos de salida del servicio
				consultaEquipoRSType.setPuertos(puertosSalida);	
				
			}

			//el listado de atributos es el mismo de la entrada
			if (listaAtribSalida!=null)
				consultaEquipoRSType.setAtributos(listaAtribSalida);

			
			//obtengo el par�metro CantidadPuertos
			consultaEquipoRSType.setCantidadPuertos(cantPuertos+"");
			
			//obtengo el detalle de la respuesta
			HashMap<String, Object> detalleRespuesta = (HashMap<String, Object>)respuesta.get("DetalleRespuesta") ;
			consultaEquipoRSType.getDetalleRespuesta().setCodigoError(ServiciosUtil.depurarParametro(detalleRespuesta,"CodigoError"));
			consultaEquipoRSType.getDetalleRespuesta().setCodigoRespuesta(ServiciosUtil.depurarParametro(detalleRespuesta,"CodigoRespuesta"));
			consultaEquipoRSType.getDetalleRespuesta().setDescripcionError(ServiciosUtil.depurarParametro(detalleRespuesta,"DescripcionError"));
			consultaEquipoRSType.getDetalleRespuesta().setMensajeError(ServiciosUtil.depurarParametro(detalleRespuesta,"MensajeError"));
			HashMap<String, GregorianCalendar> fechaSol = (HashMap<String, GregorianCalendar>) detalleRespuesta.get("FechaRespuesta");
			consultaEquipoRSType.getDetalleRespuesta().setFechaRespuesta(obtenerFechaSolicitud((GregorianCalendar) fechaSol.get("date")));
			
			
		} catch (ParameterException e) {
			System.out.println(mensajeErrorParametroInvalido);
		} catch (ServiceLocatorException e) {
			registrarException(consultaEquipoRSType.getDetalleRespuesta(), rb.getString(KeyGSSException), MessageFormat.format(ServiciosUtil.getMensajeErrorGSS(),keyNombreFuncion), e, true);
		} catch (AuthorisationException e) {
			registrarException(consultaEquipoRSType.getDetalleRespuesta(), rb.getString(KeyGSSException), MessageFormat.format(ServiciosUtil.getMensajeErrorGSS(),keyNombreFuncion), e, true);
		} catch (GSSException e) {
			registrarException(consultaEquipoRSType.getDetalleRespuesta(), rb.getString(KeyGSSException), MessageFormat.format(ServiciosUtil.getMensajeErrorGSS(),keyNombreFuncion),  e, true);
		}catch (Exception e) {
			registrarException(consultaEquipoRSType.getDetalleRespuesta(), rb.getString(KeyGeneralException), MessageFormat.format(ServiciosUtil.getMensajeErrorGeneral(),keyNombreFuncion), e, true);
		} finally {
			try {
				// Envia la respuesta al servicio
				response.addResponse("response", consultaEquipoRSType);
			} catch (ParameterException e) {
				SWLog.log(Level.ERROR, this, e.getMessage());
			}
		}

		return response;
	}

	
	/**
	 * Funci�n que valida si el campo es obligatorio. No puede ser vac�o
	 * @param nombreCampo
	 * 			Nombre del campo a validar
	 * @param valorCampo
	 * 			Valor del campo a validar
	 * @param detalleRespuesta
	 * 			Objeto donde se retorna la respuesta
	 * @throws ParameterException
	 * 			En caso que el campo est� vac�o se dispara una excepci�n
	 */
	private void validarCampoObligatorio(String nombreCampo, String valorCampo,	Detallerespuestatype detalleRespuesta) throws ParameterException {
		// valido que el campo no sea vac�o
		if (ServiciosUtil.esCampoVacio(valorCampo)) {
			//notifico la ausencia del campo obligatorio
			notificarParametroObligatorio(detalleRespuesta, nombreCampo);
		}
		
	}
	
	/**
	 * Funci�n que valida si el campo Fecha Solicitud es obligatorio. No puede ser vac�o
	 * @param nombreCampo
	 * 			Nombre del campo a validar
	 * @param fecha
	 * 			Valor del campo a validar
	 * @param detalleRespuesta
	 * 			Objeto donde se retorna la respuesta
	 * @throws ParameterException
	 * 			En caso que el campo est� vac�o se dispara una excepci�n
	 */
	private void validarCampoObligatorio(String nombreCampo, UTCDate fecha,	Detallerespuestatype detalleRespuesta) throws ParameterException {
		//valido que el objeto no sea vac�o
		if (fecha==null|| fecha.getDate()==null || ServiciosUtil.esCampoVacio(fecha.getDate().getBoundedString14())){
			//notifico la ausencia del campo obligatorio
			notificarParametroObligatorio(detalleRespuesta, nombreCampo);
		}
	}
	
	/**
	 * Procedimiento que notifica la ausencia de un par�metro obligatorio en la entrada del servicio
	 * @param detalleRespuesta
	 * 			Objeto donde se retorna la respuesta
	 * @param nombreCampo
	 * 			Nombre del campo obligatorio ausente
	 * @throws ParameterException
	 * 			En caso que el campo est� vac�o se dispara una excepci�n
	 */
	private void notificarParametroObligatorio(Detallerespuestatype detalleRespuesta, String nombreCampo)  throws ParameterException {
		//configuro mensaje del log
		mensajeErrorParametroInvalido = MessageFormat.format(ServiciosUtil.getMensajeErrorParametros(),keyNombreServicio,nombreCampo);
		//el campo es vacio, lanzo excepcion
		registrarException(detalleRespuesta, rb.getString(KeyRequiredParameterException), MessageFormat.format(ServiciosUtil.getMensajeErrorParametrosObligatorios(), keyNombreFuncion), null, false);
		lanzarParameterException();
	}
	
	/**
	 * Lanzador de excepciones utilzado cuando fallas en la entrada de par�metros de los servicios
	 * @throws ParameterException
	 * 				Informa de la falla ocurrida
	 */
	private static void lanzarParameterException() throws ParameterException{
		Throwable t = new Throwable();
		ParameterException e = new ParameterException(t);
		throw e;
	}
	
	/**
	 * @param generalType
	 * 			Objeto de respuesta del proceso del servicio
	 * @param codigoParam
	 * 			Mensaje del c�digo de error
	 * @param mensaje
	 * 			Mensaje del error
	 * @param e
	 * 			Excepcion a reportar
	 * @param mostrarTrace
	 * 			Verdadero para mostrar el traceRoot de la Excepcion, falso en cualquier otro caso
	 */

	private void registrarException(Detallerespuestatype generalType, String codigoParam, String mensaje, Exception e, boolean mostrarTrace){
		
		//Lleno los par�metros de respuesta
		generalType.setCodigoError(codigoParam);
		generalType.setDescripcionError(mensaje);
		generalType.setMensajeError(mensaje);
		
		//Si mostrarTrace es TRUE se muestra el traceRoot
		if (mostrarTrace) e.printStackTrace();
		
	}

	/**
	 * Funci�n que inicializa los par�metros de salida del servicio
	 * @param consultaEquipoRSType
	 * 			Par�metros de salia a inicializar
	 */
	private void inicializarParametrosSalida(WsM6ConsultarEquipoRSType consultaEquipoRSType) {
		//inicializo idEquipoUNE
		consultaEquipoRSType.setIdUneEquipo("");
		//inicializo TipoEquipo
		consultaEquipoRSType.setTipoEquipo("");
		//inicializo CantidadPuertos
		consultaEquipoRSType.setCantidadPuertos("0");
		
		//creo el detalle de la respuesta
		Detallerespuestatype detalleRespuesta = new Detallerespuestatype();
		//inicializo c�digo de Error
		detalleRespuesta.setCodigoError("");
		//inicializo c�digo de respuesta, por defecto es falla
		detalleRespuesta.setCodigoRespuesta(ServiciosUtil.KO);
		//inicializo descripci�n del error
		detalleRespuesta.setDescripcionError("");
		//creo la fecha de respuesta, por defecto la fecha del sistema
		UTCDate utcDate = new UTCDate();
		//obtengo la fecha del sistema
		Date ahora = new Date();
		//le doy formato la fecha del sistema
		SimpleDateFormat format = new SimpleDateFormat(ServiciosUtil.getFormatoFechaDefault());
		BoundedString14 boundedString14 = new BoundedString14();
		boundedString14.setBoundedString14(format.format(ahora.getTime()));
		utcDate.setDate(boundedString14);
		//asigno la fecha
		detalleRespuesta.setFechaRespuesta(utcDate);
		//inicializo mensaje del error
		detalleRespuesta.setMensajeError("");
		//asigno la respuesta a la salida
		consultaEquipoRSType.setDetalleRespuesta(detalleRespuesta);
		
		//inicializo la fecha de la solicitud, en caso que falte alg�n par�metro obligatorio
		consultaEquipoRSType.setFechaSolicitud(utcDate);
	}
	
	/**
	 * Funci�n que transforma la fecha de Calendario Gregoriano a formato UTCDate
	 * @param fecha
	 * 		fecha a transformar
	 * @return
	 * 		fecha en formato UTCDate
	 */
	private UTCDate obtenerFechaSolicitud(GregorianCalendar fecha) {

		UTCDate utcDate = new UTCDate();
		SimpleDateFormat format = new SimpleDateFormat(ServiciosUtil.getFormatoFechaDefault());
		BoundedString14 boundedString14 = new BoundedString14();
		boundedString14.setBoundedString14(format.format(fecha.getTime()));
		utcDate.setDate(boundedString14);
		return utcDate;

	}

}
