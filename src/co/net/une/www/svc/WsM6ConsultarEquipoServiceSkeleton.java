
/**
 * WsM6ConsultarEquipoServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsM6ConsultarEquipoServiceSkeleton java skeleton for the axisService
     */
    public class WsM6ConsultarEquipoServiceSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsM6ConsultarEquipoServiceLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param fechaSolicitud
                                     * @param idUneEquipo
                                     * @param tipoEquipo
                                     * @param atributos
         */
        

                 public co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType consultarEquipo
                  (
                  co.net.une.www.ncainvm6.UTCDate fechaSolicitud,java.lang.String idUneEquipo,java.lang.String tipoEquipo,co.net.une.www.ncainvm6.Listaatributostype atributos
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("fechaSolicitud",fechaSolicitud);params.put("idUneEquipo",idUneEquipo);params.put("tipoEquipo",tipoEquipo);params.put("atributos",atributos);
		try{
		
			return (co.net.une.www.ncainvm6.WsM6ConsultarEquipoRSType)
			this.makeStructuredRequest(serviceName, "consultarEquipo", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    