/*
 * Generated by XDoclet - Do not edit!
 */
package co.net.une.www.interfaces;

/**
 * Local interface for WsM6ConsultarEquipoService.
 * @creado 19/02/2016
 * @ultimamodificacion 19/02/2016
 * @historial 19/02/2016 FJM Primera versi�n
 * @xdoclet-generated at ${TODAY}
 * @copyright The XDoclet Team
 * @author XDoclet
 * @version ${version}
 */
public interface WsM6ConsultarEquipoServiceLocal
   extends javax.ejb.EJBLocalObject, com.gesmallworld.gss.lib.service.ChainableServiceLocal
{
   /**
    * Generated service proxy method. Corresponds to a service of a Magik Service Provider.
    * @param request A request object as specified in the service description file.
    * @return A response instance    */
   public com.gesmallworld.gss.lib.request.Response consultarEquipo( com.gesmallworld.gss.lib.request.Request request ) ;

}
